---
title: The ownCloud Public Link Creator
author: Björn Schießle
type: post
date: 2013-12-30T09:28:03+00:00
slug: the-owncloud-public-link-creator
tmac_last_id:
  - 735035417754533888
categories:
  - english
tags:
  - bash
  - hacking
  - owncloud
  - thunar

---

<div class="article-image" style="float: right; width: 300px">
<img src="/img/articles/owncloud-public-link-creator-context-menu.png" width=300px/>
<p class="image-description">
    ownCloud Share Link Creator &#8211; Context Menu
  </p>
</div>

Holiday season is the perfect time to work on some stuff on your personal ToDo list. ownCloud 6 introduced a public REST-style [Share-API][1] which allows you to call various share operations from external applications. Since I started working on the Share-API I thought about having a simple shell script on my file manager to automatically upload a file and generate a public link for it&#8230; [Here it is!][2]

I wrote a script which can be integrated in the Thunar file manager as a &#8220;custom action&#8221;. It is possible that the program also works with other file managers which provide similar possibilities, e.g Nautilus. But until now I tested and used it with Thunar only. If you try the script with a different file manager I would be happy to hear about your experience.

<div class="article-image" style="float: left; width: 300px">
<img src="/img/articles/owncloud-public-link-creator-upload.png" width=300px/>
<p class="image-description">
    ownCloud Share Link Creator &#8211; File Upload
  </p>
</div>

If you configure the &#8220;custom action&#8221; in Thunar, make sure to pass the paths of all selected files to the program using the &#8220;%F&#8221; parameter. The program expects the absolute path to the files. In the &#8220;Appearance and Conditions&#8221; tab you can activate all file types and directories. Once the custom action is configured you can execute the program from the right-click context menu. The program works for all file types and also for directories. Once the script gets executed it will first upload the files/directories to your ownCloud and afterwards it will generate a public link to access them. The link will be copied directly to your clipboard, additionally a dialog will inform you about the URL. If you uploaded a single file or directory than the file/directory will be created directly below your default target folder as defined in the shell script. If you selected multiple files, than the program will group them together in a directory named with the current timestamp.

This program does already almost everything I want. As already said, it can upload multiple files and even directories. One think I want to add in the future is the possibility to detect a ownCloud sync folder on the desktop. If the user selects a file in the sync folder than the script should skip the upload and create the share link directly.

**Edit:** In the meantime I got feedback that the script also works nicely with Dolphin, Nautilus and Nemo

 [1]: http://doc.owncloud.org/server/5.0/developer_manual/core/ocs-share-api.html
 [2]: https://github.com/schiesbn/shareLinkCreator