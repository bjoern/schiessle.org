---
title: Introduction to the new ownCloud Encryption App
author: Björn Schießle
type: post
date: 2013-05-28T15:08:13+00:00
slug: introduction-to-the-new-owncloud-encryption-app
tmac_last_id:
  - 735035419658735617
categories:
  - english
tags:
  - owncloud
  - encryption

---
Last weekend we [released][1] a first preview version of the new encryption app. This wouldn&#8217;t be possible without the work done by Sam Tuke and Florin Peter. Thanks a lot for all your work! Let me take the opportunity to tell you some details about the app, what it does and how it works.

The encryption app for ownCloud 5 was a complete re-write. We moved from the relatively weak blowfish algorithm to the more secure AES algorithm. The complete encryption is built on top of OpenSSL a well-known and tested encryption library. Further, the encryption app is integrated into ownCloud seamlessly. This means that the encrypt and decrypt happens transparently so that you can still use all the other features from ownCloud like sharing, different viewer apps, WebDAV access etc.

To make this possible, we decided to perform the encryption server-side. Still the architecture allows us to implement client-side encryption as an additional option later. Server-side encryption is especially interesting for users who also use the external storage app. Combining the external storage app with the encryption app allows you to use external storage without giving any 3rd-party provider access to your data.

ownCloud uses the users log-in password for encryption. This means that you should choose a strong password in order to protect your data. It is important to know that by default a user will lose access to his data if he loses his log-in password. As an additional feature the administrator can generate a recovery key which allows him to recover user data. Once this feature is activated in the administrator settings every user can enable the recovery key in his personal settings. By default the recovery key is disabled. Every user can decide for himself whether he wants this additional protection against password loss or not. Since we are using server-side encryption this feature does not reduce the security. Keep in mind that your ownCloud administrator will always be able to intercept your data because everything gets encrypted and decrypted at the server. Since ownCloud is Free Software you can choose a trustworthy administrator freely or decide to be your own administrator if you wish.

Let&#8217;s talk about some technical details and how the encryption works. The encryption is based on three different keys: every user has a private/public _key-pair_, every file has a _file-key_ and to give multiple users access to a file we have _share-keys_.

Every user has an asymmetric 4096-bit strong _key-pair_ which consists of a private and a public key. The private key is encrypted with the users log-in password, for the encryption AES-128 is used. Additionally there are up to two system-wide key-pairs: One for public link shares which allows ownCloud to decrypt files which are shared as public link and if enabled the recovery-key-pair.

In order to not always have to encrypt and decrypt large files we have introduced the _file-keys_ which are 183 byte strong ASCII keys. The file-key is used to encrypt the users file symmetrically with AES-128. Than the file-key gets encrypted with the public keys from all users with access to the file. This means that if a user gets added or removed from a file we only have to re-encrypt the small file-key instead of the whole file.

Every time a file-key gets encrypted to multiple users OpenSSL generates for each user an additional _share-key_. Only the combination of the users private key with the corresponding share-key enables the user to decrypt the given file again.

Everybody is welcome to test the new encryption app and report issues on our [mailing list][2] or preferable directly on [GitHub][3]. But keep in mind that this is a preview version, you should always have a backup of your unencrypted data!

 [1]: https://mail.kde.org/pipermail/owncloud/2013-May/009270.html
 [2]: https://mail.kde.org/mailman/listinfo/owncloud
 [3]: https://github.com/owncloud/core/