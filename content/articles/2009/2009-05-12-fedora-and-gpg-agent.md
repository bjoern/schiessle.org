---
title: Fedora and gpg-agent
author: Björn Schießle
type: post
date: 2009-05-12T16:28:37+00:00
slug: fedora-and-gpg-agent
categories:
  - english
tags:
  - bash
  - fedora
  - gnupg
  - smartcard
  - ssh

---
While it was quite easy to set up my [Fellowship][1] smartcard for SSH logins on Debian GNU/Linux following this [instructions][2] I never managed to get it working on Fedora GNU/Linux. At some point of time I just gave up. Today finally I found a solution in an on-line forum.

The problem was that gpg-agent always stopped with the error message:

<pre>$ gpg-agent 
gpg-agent[2857]: can't connect to `/home/schiesbn/.gnupg/S.gpg-agent': No such file or directory
gpg-agent: no gpg-agent running in this session
</pre>

By default the gpg-agent on Fedora creates the socket in /tmp instead of in /home/schiesbn/.gnupg. So you have to move it manually over to your home directory once gpg-agent has started.

To do this I use this script:

<pre lang="bash" line="0">#!/bin/bash

# Decide whether to start gpg-agent daemon.
# Create necessary symbolic link in $HOME/.gnupg/S.gpg-agent

SOCKET=S.gpg-agent
PIDOF=`pidof gpg-agent`
RETVAL=$?

if [ "$RETVAL" -eq 1 ]; then
	echo "Starting gpg-agent daemon."
	eval `gpg-agent --daemon `
else
	echo "Daemon gpg-agent already running."
fi

# Nasty way to find gpg-agent's socket file...
GPG_SOCKET_FILE=`find /tmp/gpg-* -name $SOCKET`
echo "Updating socket file link."
cp -fs $GPG_SOCKET_FILE $HOME/.gnupg/S.gpg-agent
</pre>

To execute this script during log-in I have added this to my ~/.bashrc:

<pre lang="bash" line="0"># GPG-AGENT stuff
GET_TTY=`tty`
export $GET_TTY
$HOME/bin/gpg-agent-start.sh
</pre>

I still wonder why it works that easy on Debian and on Fedora i need all this scripting. But for the moment I&#8217;m just happy that I have found a solution to use my smartcard for SSH login on my Fedora systems.

 [1]: http://fellowship.fsfe.org
 [2]: http://blogs.fsfe.org/greve/?p=64