---
title: Jabber Mail Notification
author: Björn Schießle
type: post
date: 2009-02-20T16:55:29+00:00
slug: jabber-mail-notification
categories:
  - english
tags:
  - hacking
  - xmpp

---
I always struggled to find the right mail notification applet for my desktop. Furthermore I always stumble over the question: Why do I have to ask the mail server in a defined time interval &#8220;Do I have a new e-mail?&#8221;. Wouldn&#8217;t it be better if the mail server notifies me if a new e-mail arrives?
  
This is probably somehow a new form of the good old question &#8220;mailing list vs bulletin board&#8221; or in general: Do i have to fetch the information or does the information come to me? Personally i always preferred to get the information and not to hunt around for them.

Thinking about this question i realized that notification through [Jabber][1] would be perfect and the open [XMPP][2] protocol virtually invites one to do such things.

The idea was born. Now the first step was to find a easy to use XMPP implementation for a scripting language like Python, Ruby or PHP. At the end I found a quite nice and easy to use [PHP library][3]. While searching such a library I also found this [guidance (German only)][4], borrowed some code from it and my solution was born:

<pre lang="PHP" line="1"><?php
// The script gets the input either as an argument, from a REQUEST-variable or from stdin
// If you use it within procmail you will get the input through stdin 
if ($argv[1]) {
    $msg = $argv[1];
} elseif ($_REQUEST['msg']) {
    $msg = urldecode($_REQUEST['msg']);
} else {
    // open stdin. Only read the first 4096 character, this should be enough to match
    // the FROM- and  SUBJECT-header
    $stdin = fopen('php://stdin', 'r');
    $msg   = fread($stdin, 4096);

    if (empty($msg)) {
        $msg = "empty";
    } else {
        // Get FROM und SUBJECT
        preg_match('@From:(.*)@i', $msg, $from);
	preg_match('@Subject:(.*)@i', $msg, $subject);
        $msg = "\n" . $from[0] . "\n" . $subject[0] . "\n";
    }
}

// now init xmpp and get the notification out
include 'XMPPHP/XMPP.php';

$conn = new XMPPHP_XMPP('schiessle.org', 5222, 'user', 'password', 'xmpphp', 'schiessle.org', $printlog=false, $loglevel=XMPPHP_Log::LEVEL_INFO);

try {
    $conn->connect();
    $conn->processUntil('session_start');
    $conn->presence();
    $conn->message('me@jabber.server.org', $msg);
    $conn->disconnect();
} catch(XMPPHP_Exception $e) {
    die($e->getMessage());
}
?>
</pre>

Now I just had to tell procmail to pipe the mails through the PHP script. If you want to get notified about all mails you can simply put this line at the top of your procmail rules (Or maybe at least behind the spam filter rules 😉 ):

<pre lang="bash" line="1">:0c
|php /home/schiessle/bin/mailnotification.php
</pre>

I want to get notified only about some specific mails so I extended my procmail rules in this way:

<pre lang="bash" line="1">:0
* ^(To:|Cc:).*foo@bar-mailinglist.org
{
 	:0c
        |php /home/schiessle/bin/mailnotification.php

        :0
        .bar-list/
}
</pre>

That&#8217;s it! All in all it was quite easy to get e-mail notification through Jabber. Now I don&#8217;t have to search for the right applet, configure it etc.. All I have to do is to start my Jabber client and I will get notified about new mails whatever desktop or computer i&#8217;m using.

 [1]: http://www.jabber.org
 [2]: http://xmpp.org
 [3]: http://code.google.com/p/xmpphp/
 [4]: http://garv.in/serendipity/archives/947-Jabber-E-Mail-Notify.html