---
title: Happy Birthday FSFE!
author: Björn Schießle
type: post
date: 2009-03-11T21:10:58+00:00
slug: happy-birthday
categories:
  - english
tags:
  - fsfe

---
Today the Free Software Foundation Europe (FSFE) [celebrates][1] his 2<SUP>3</SUP> birthday! For 8 years now, the FSFE works for basic rights and freedom in the digital age, a world in which our life increasingly depends on software.

In this eight years FSFE achieved a lot, from the local area to Europe and even up to the United Nations. It ranges from highly visible activities like the [Document Freedom Day][2] and the recently launched [pdfreaders.org][3] to a lot of behind-the-scenes activities like the work at the United Nations, promoting Free Software interests at the World Summit on the Information Society (WSIS), contributed to the Internet Governance Forum (IGF), taken part in discussions at the World Intellectual Property Organisation (WIPO), taught project managers of the World Bank about Free Software and many more.

In honor of the day FSFE has released a special edition of the Fellowship Interview series, in which Georg Greve explains the history behind FSFE and how he came to found it. [Here][4] you can find the Interview.

 [1]: http://www.fsfeurope.org/news/2009/news-20090311-01.en.html
 [2]: http://www.documentfreedom.org
 [3]: http://www.pdfreaders.org
 [4]: http://fellowship.fsfe.org/interviews/greve/