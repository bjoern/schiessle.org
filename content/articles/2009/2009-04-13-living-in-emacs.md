---
title: Living in Emacs
author: Björn Schießle
type: post
date: 2009-04-13T16:36:10+00:00
slug: living-in-emacs
categories:
  - english
tags:
  - emacs
  - gnu
  - gnus
  - org-mode

---
E-Mail, News, Editing, Writing, Programming,&#8230; Since a few year now I use [GNU][1] [Emacs][2] for almost everything i do regularly on my computer. But there is still so many to discover and learn. The last few weeks I started to use [org-mode][3] for notes, ToDo lists, etc and i really love it. Especially the feature to link from your ToDo list or note directly to an mail in [Gnus][4]. Just copy (C-c l) the link to the mail in Gnus and insert (C-c C-l) it into the note. Now I can click on the link in my ToDo list and Emacs will show me directly the corresponding mail/thread in my mail client. Great!

As already said, for mails and news I use Gnus. Until now I always set up postfix as my Mail Transport Agent (MTA). Only today I learnt that Gnus is able to send e-mails directly without an external MTA. The only pitfall, you have to install gnutls-bin and starttls to use SSL/TLS. Otherwise it will not work and you will not get a meaningful error message. After that it is fairly easy. I just had to enter this into my ~/.gnus:

<pre lang="lisp" line="1">(setq smtpmail-smtp-server "smtp-server")
(setq smtpmail-smtp-service 25)
(require 'smtpmail)
(setq message-send-mail-real-function 'smtpmail-send-it)
(setq smtpmail-auth-credentials
 '(("smtp-server" 25 "user" "passwd")))
(setq smtpmail-starttls-credentials
     '(("smtp-server" 25 nil nil)))
(setq starttls-use-gnutls t)
(setq starttls-gnutls-program "gnutls-cli")
(setq starttls-extra-arguments nil)
</pre>

That&#8217;s it. Now I can apt-get remove postfix and have one service less in the background of my daily computing.

 [1]: http://www.gnu.org
 [2]: http://www.gnu.org/software/emacs
 [3]: http://orgmode.org/
 [4]: http://www.gnus.org/