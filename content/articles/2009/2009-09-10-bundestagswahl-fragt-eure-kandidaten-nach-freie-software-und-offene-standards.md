---
title: 'Bundestagswahl: Fragt eure Kandidaten nach Freie Software und Offene Standards!'
author: Björn Schießle
type: post
date: 2009-09-10T12:28:56+00:00
slug: bundestagswahl-fragt-eure-kandidaten-nach-freie-software-und-offene-standards
categories:
  - deutsch
tags:
  - election
  - FreeSoftware
  - fsfe
  - OpenStandards
  - policy
  - swpat

---
Es ist wieder mal so weit, nur noch wenige Tage bis zur Bundestagswahl. Am 27. September sind wir alle dazu aufgerufen, unser Kreuzchen zu setzen und damit den Bundestag für die nächsten 4 Jahre zu wählen. In kaum einer anderen Zeit sind die Politiker so gesprächsbereit und auskunftsfreudig, dies will auch die [Free Software Foundation Europe (FSFE)][1] nutzen und ruft dazu auf, [die Kandidatinnen/Kandidaten zur Bundestagswahl nach ihren Positionen zu Freier Software und Offenen Standards zu fragen][2]. 

Bereits eingegangene Antworten werden im [Wiki][3] gesammelt und veröffentlicht. Dort kann man auch eine Anleitung finden, um sich selber an der Aktion zu beteiligen.

Ein paar Antworten wurden schon gesammelt, darunter auch die des SPD Kanzlerkandidaten Frank-Walter Steinmeier:

> &#8220;Stichwort Kreativwirtschaft: Andermann hat mich in seinem Blog-Beitrag aufgefordert, mehr für freie Software zu tun. Als Außenminister kann ich darauf verweisen, dass das Auswärtige Amt Vorreiter beim Einsatz freier Software ist. Auch in meinem Deutschland-Plan setze ich auf innovative Lizenzformen. Ich will, dass alle die Möglichkeit haben, mitzumachen – auch und gerade im Netz.&#8221;

Außerdem erwähnt er Freie Software und Offene Standards im [Impressum][4] seiner Internetpräsenz.

Von den Grünen gab es bisher vor allem Aussagen zum Thema Softwarepatente. So sagt Sibyll Klotz:

> &#8220;Software ist durch das Urheberrecht angemessen geschützt.&#8221; Software lässt sich nicht in die gängigen Patentübereinkommen integrieren. Patente nutzen den großen Unternehmen (die über eine eigene Patent- und Rechtsabteilung verfügen) und schaden kleinen Unternehmen. Dies ist schädlich für Wettbewerb und Innovation und führt dazu, dass Software teurer und in ihrer Vielfalt eingeschränkt wird. Außerdem schaden Softwarepatente Freier bzw. Open Source Software weil eine Geheimhaltung bis zur Patentanmeldung verlangt wird. Eine schleichende Ausweitung der Patentierbarkeit muss verhindert werden.

Ihr Parteikollege Friedrich Ostendorff antwortete:

> &#8220;Patente können innovationshemmend sein. Staatlich finanzierte Forschungsergebnisse müssen so frei wie möglich lizensiert werden. Die Grünen unterstützen Freie Software.&#8221; 

Macht mit und befragt auch euren Kandidaten zu den Themen [Freie Software][5] und [Offene Standards][6]!

 [1]: http://fsfe.org
 [2]: http://www.fsfe.org/news/2009/news-20090908-01
 [3]: http://wiki.fsfe.org/Bundestagswahl2009
 [4]: http://www.frankwaltersteinmeier.de/servicenavigation/impressum/index.html
 [5]: http://www.fsfe.org/about/basics/freesoftware
 [6]: http://www.fsfe.org/projects/os/def
