---
title: My Backup Solution
author: Björn Schießle
type: post
date: 2009-07-16T08:07:56+00:00
slug: my-backup-solution
categories:
  - english
tags:
  - bash
  - hacking

---
For a long time I have made backups of my home partition by hand, starting from time to time [rdiff-backup][1]. But as you can imagine, this approach doesn&#8217;t generate regular and reliable backups.

I couldn&#8217;t put this task into a simple cronjob because of two reasons. First I use encrypted hard disks and my backup disk is connected via USB and not always on. So before a backup starts I have to turn on my backup disk and make sure, that my home partition and my backup disk is decrypted and mounted. Second I don&#8217;t want the backup happen during my regular work. In my experience such processes often starts in the most annoying moments.

So I decided that I need an semi-automatic backup, which runs during shutdown. The result is this small script which I put in /etc/rc0.d/K05backup.sh:

<pre lang="bash" line="1">#!/bin/bash

currentTime=`date +%s`
timeUntilNextBackup=604800                 # 604800sec = 1week
startBackup=false

# check if it's time for the next backup
if [ -f /var/log/nextBackup.log ]; then
    nextBackupTime=`cat /var/log/nextBackup.log`
    if [ $(($currentTime - $nextBackupTime)) -gt 0 ]; then
        startBackup=true                       #time for the next backup
    fi
else
    startBackup=true
fi

if [ $startBackup == true ]; then
    echo "It's time for another Backup!"
    echo "Don't forget to switch on your backup hard disk before you start!"
    repeat=true
    while $repeat; do
        echo -n "Start backup procedure now? (y)es or (n)o? "
        read char
        case $char in
            [y,Y] ) 
                if [ ! -d /home/schiesbn ]; then
                    echo "encrypted HOME partition has to be mounted..."
                    cryptsetup luksOpen /dev/sda6 secureHome
                    mount /dev/mapper/secureHome /home
                fi
                echo "encrypted BACKUP partition has to be mounted..."
                cryptsetup luksOpen /dev/sdd1 secureBackup
                mount /dev/mapper/secureBackup /mnt/backup
                echo "Starting Backup...";
                rdiff-backup --print-statistics /home/schiesbn /mnt/backup
                echo "umount backup disk..."
                umount /mnt/backup
                cryptsetup luksClose secureBackup
                # calculate the time for the next backup and write it to the log
                nextBackup=$(($currentTime + $timeUntilNextBackup))
                echo $nextBackup > /var/log/nextBackup.log
                echo "DONE."
                sleep 10   #give me some time to look at the backup statistics
                repeat=false;;
            [n,N] )
                repeat=false;;
        esac
    done
fi
</pre>

If the last backup is older than 1 week the script asks me, if I want to do another backup. Than I can decide to postpone it or to start it now. If I decide to start the backup procedure I get the opportunity to decrypt my backup and home partition before rdiff-backup starts. After that I can leave the room and be sure that the computer will shutdown after the backup is finished.

Until now this is the best and most reliable, least annoying and most automated solution I could found.

 [1]: http://rdiff-backup.nongnu.org/