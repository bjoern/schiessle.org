---
title: Free Software in Schools
author: Björn Schießle
type: post
date: 2009-05-20T22:43:32+00:00
slug: free-software-in-schools
categories:
  - english
tags:
  - education
  - FreeSoftware

---
At the moment Free Software in education is one of my main interests in the Free Software ecosystem. [FSFE&#8217;s Fellowship wiki][1] already provides some useful information on this topic. It&#8217;s quite interesting to see the development in this area. Further I think schools play an important role for the adoption of Free Software in many areas. What pupils learn and get used to during school is what they want to use and what they demand if they enter the business world. I also think that it is important to get as early as possible at least a basic idea about the role of software in the information society. 

[<img src="/img/articles/linux-advances-distribution-for-education.jpg" class="article-image center" />][2]

Today I read an interesting article about a school in Austria which uses [GNU/Linux systems on USB sticks (German only)][3]. With [LinuxAdvanced][4] the school created their own distribution based on Debian GNU/Linux 5.0 (aka Lenny) and the lightweight desktop environment Xfce.

The Kremser Bundesgymnasium uses this system since two years on all computers in the computer science classrooms. Now they decided to switch from local installations to live systems on USB sticks. The advantage: The pupils can carry their system around with themselves. They can use it at school, at home or at any computer they want. About 50% of all pupils uses the system regularly at home. It seems like especially the young pupils using the system quite naturally and have no reservations. Further Rene Schwarzinger explains: &#8220;We don&#8217;t want to encourage our pupils to create illegal copies just to be able to work at home with the same programs as at school&#8221;. The obvious solution to avoid this is to use only [Free Software][5] at school and pass it down to the pupils.

In autumn they want to introduce netbooks together with the GNU/Linux USB stick to the pupils.

I really like the idea using USB sticks instead of normal installations on hard disks. Live systems are nothing new but I think it makes much sense in this scenario. With the USB sticks the pupils can work with their systems and their data wherever they want without having to convince their parents to install a new operating system at home which could be quite challenging, both technically and philosophically.

I&#8217;m interested in more success stories about Free Software and GNU/Linux in schools. Please let me know if you know schools (especially in Germany or Europe) which already uses GNU/Linux or prepare the switch to Free Software.

 [1]: http://wiki.fsfe.org/Education
 [2]: http://www.linuxadvanced.at/grafiken/albumshaper/LinuxAdvanced/subalbum_1.html
 [3]: http://futurezone.orf.at/stories/1603209/
 [4]: http://www.linuxadvanced.at
 [5]: http://www.fsfe.org/documents/freesoftware
