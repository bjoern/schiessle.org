---
title: Fellowship-Treffen und KDE 4.2 Release-Party at Stuttgart
author: Björn Schießle
type: post
date: 2009-01-17T13:30:46+00:00
slug: fellowship-treffen-und-kde-42-release-party-in-stuttgart
categories:
  - deutsch
tags:
  - fsfe
  - kde
  - stuttgart

---
<img src="/img/articles/fsfe-plussy.png" class="article-image" style="float: left;" />
Am Freitag, den 30. Januar findet um 18:00 Uhr wieder ein [Fellowship][1]-Treffen in Stuttgart statt. Veranstaltungsort wird das Unithekle/Unitop an der Universität Stuttgart (Campus Vaihingen) sein.

Erreichen könnt ihr das Unithekle mit den S-Bahn Linien S1, S2 und S3, Haltestelle Universität, oder direkt mit dem Auto. Einen genauen Lageplan und weitere Informationen findet ihr [hier][2].

<img src="/img/articles/kde-logo.jpg" class="article-image" style="float: right;" />
Ich freue mich ganz besonders, euch diesmal nicht nur im Namen der [FSFE][4], sondern auch im Namen von [KDE][3] einladen zu dürfen. Am 27. Januar wird KDE4.2 veröffentlicht, und wir wollen dies am 30. gemeinsam feiern. Es werden auch einige KDEler anwesend sein, ich freue mich auf Frederik Gladhorn (KDE), Lydia Pintscher (Amarok) und Frank Karlitschek (kde-apps.org).

Als Programmpunkte werden wir einen kleinen Überblick über die Arbeit der FSFE und die Aktivitäten des Fellowship haben sowie eine Vorstellung von KDE4.2 und einigen Anwendungen.

Darüber hinaus werden wir natürlich genug Zeit haben, um in gemütlicher Atmosphäre die Veröffentlichung von KDE4.2 zu feiern und uns über alle möglichen Themen rund um Freie Software auszutauschen.

Eingeladen sind alle, die sich für [Freie Software][5] interessieren.

 [1]: http://fellowship.fsfe.org
 [2]: https://wiki.fsfe.org/FellowshipGroup/Stuttgart
 [3]: http://www.kde.org
 [4]: http://www.fsfeurope.org
 [5]: http://www.fsfeurope.org/documents/freesoftware.de.html