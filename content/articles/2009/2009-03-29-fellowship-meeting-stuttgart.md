---
title: Fellowship meeting Stuttgart
author: Björn Schießle
type: post
date: 2009-03-29T20:36:47+00:00
slug: fellowship-meeting-stuttgart
categories:
  - english
tags:
  - education
  - fsfe
  - stuttgart

---
<img src="/img/articles/fsfe-plussy.png" class="article-image" style="float: left" />It&#8217;s time for the next [Fellowship][1] meeting in Stuttgart. It will take place at Thursday, 2 April at 19:00! We are going to meet in the &#8220;Unithekle&#8221;, Allmandring 17 , 70596 Stuttgart. Short term changes are announced in the [wiki][2]. 

This time we want to talk about [Free Software in education][3]. I think this is an interesting and important topic. We live in the digital age. Therefore pupils should learn how to use and develop information technology in an social and sustainable way and all pupils should have equal opportunities. So if you know pupils, teachers or parents which are interested in [Free Software][4], just take them with you to the Fellowship meeting.

I&#8217;m sure we will also have enough time to talk about other interesting Free Software topics. So if you are interested in Free Software, the FSFE and/or Free Software in education come and meet us at Thursday, 2. April.

For more information, also about previous events, look at our [wiki page][2].

 [1]: http://fellowship.fsfe.org
 [2]: http://wiki.fsfe.org/groups/Stuttgart
 [3]: http://wiki.fsfe.org/Reasons_for_schools_to_use_Free_Software
 [4]: http://www.fsfeurope.org/documents/freesoftware.en.html