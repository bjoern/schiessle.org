---
title: Die FSFE braucht Dich!
author: Björn Schießle
type: post
date: 2009-11-14T22:28:33+00:00
slug: die-fsfe-braucht-dich
categories:
  - deutsch
tags:
  - fsfe

---
**_Die [Free Software Foundation Europe (FSFE)][1] leistet mittlerweile seit 2001 erfolgreiche Arbeit für [Freie Software][2] in Europa._** Viele wichtigen Ziele wurden in den Jahren erreicht, vom erfolgreichen [aufbrechen von Monopolen][3] über die abwehren von [Softwarepatenten][4] bis hin zum Aufbau der [Freedom Task Force][5], Europas führende Netzwerk zu Rechtsfragen Rund um Freier Software. All dies war nur durch die großartige Unterstützung möglich, die die FSFE bisher erfahren hat.

Die erfolgreiche Arbeit über die letzten acht Jahre bedeutet aber nicht, dass keine Aufgaben mehr vor uns liegen würden. Es gibt zum Beispiel im Bereich [Offene Standards][6] noch viel zu tun, eine Schlüsselkomponente die den Wechsel hin zu Freier Software oft erst ermöglicht, [Freie Software in Bildung][7] soll in Zukunft wieder stärker thematisiert werden und auch Hersteller proprietärer Software versuchen weiterhin alles, um ihre Interessen in Europa durchzusetzen.

**_Damit sich die FSFE auch in den kommenden Jahren so erfolgreich für Freie Software und für Deine Rechte in der Informationsgesellschaft einsetzen kann [braucht sie Deine Unterstützung][8]!_**

Es gibt viele Wege die FSFE zu unterstützen, eine Möglichkeit sind [Geld-][9] und [Sachspenden][10]. Außerdem wurden speziell für Firmen [Dienstleistungen][11] zusammengestellt, durch dessen Kauf man die FSFE unterstützen kann.

Wenn Du deine Bücher oder Weihnachtsgeschenke bei [Bookzilla.de][12] kaufst, dann kann Du damit ganz einfach nebenher Freie Software und die FSFE untersützen. Vielleicht empfiehlst Du auch deinen Freunden und deiner Familie ihre zukünftigen Online-Bucheinkäufe auf diesem Weg zu tätigen?

Für Einzelpersonen ist das [Fellowship][13] eine sehr gute Möglichkeit die FSFE zu unterstützen. Damit leistet man wichtige Unterstützung in drei Bereichen: Finanziell, politisches Gewicht und aktive Beteiligung zum Beispiel in der Form von [Fellowship-Gruppen][14]. Im Rahmen des Fellowship entstanden bereits so wichtige und erfolgreiche Aktivitäten wie [PDFreaders.org][15]. Sei auch Du ein Teil davon!

Für Leute, die sich für Freie Software einsetzen und gerne ein Teil der Fellowship-Gemeinschaft wären, sich die finanzielle Komponente des Fellowship aber nicht leisten können, hat die FSFE eine Möglichkeit geschaffen über das nächste Jahr hinweg insgesamt [36 Fellowship-Stipendien][16] zu vergeben.

Das ist aber bei weitem nicht alles. Man kann die Arbeit der FSFE auch einfach durch aktives Mitarbeit unterstützen. Dafür gibt es viele [Möglichkeiten][17].

**_Eine Möglichkeit die ich dabei besonders herausstellen will ist die Beteiligung am [deutschen Übersetzerteam][18]._** Eine Organisation wie die FSFE erzeugt im Laufe ihrer Arbeit sehr viel Material und Informationen über die aktuellen Themen Rund um Freie Software. Für eine erfolgreiche politische Arbeit in Europa und um diese Informationen so vielen Menschen wie möglich zugänglich zu machen ist es sehr wichtig, diese in so vielen Sprachen wie möglich vorzuhalten.
  
Wenn ihr euch die Links in diesem Beitrag angesehen habt, dann werdet ihr sicher bemerkt haben, dass leider nicht mehr jede deutsche Übersetzung aktuell ist und die eine oder andere Übersetzung sogar ganz fehlt. Egal ob neue Texte Übersetzen, bestehende Übersetzungen aktuell halten oder Übersetzungen von anderen Korrektur lesen, es gibt immer was zu tun!
  
Ich würde mich freuen, wenn ich in den nächsten Wochen und Monaten bekannte Namen wieder oder neue Mitglieder im deutschen Übersetzerteam begrüßen dürfte. Wenn Du dazu irgendwelche Fragen hast, dann kannst Du diese jeder Zeit auf der [Mailingliste für Übersetzer][19] stellen oder mich einfach direkt ansprechen. Mehrere Möglichkeiten um mit mir in Kontakt zu treten findest Du [hier][20].

 [1]: http://www.fsfe.org
 [2]: http://www.fsfe.org/about/basics/freesoftware.de.html
 [3]: http://www.fsfe.org/projects/ms-vs-eu/ms-vs-eu.de.html
 [4]: http://www.fsfe.org/projects/swpat/swpat.de.html
 [5]: http://www.fsfe.org/projects/ftf/ftf.de.html
 [6]: http://www.fsfe.org/projects/os/os.de.html
 [7]: http://fsfe.org/projects/education/education.de.html
 [8]: http://www.fsfe.org/donate/letter-2009.de.html
 [9]: http://www.fsfe.org/donate/donate.de.html
 [10]: http://www.fsfe.org/donate/hardware.de.html
 [11]: http://www.fsfe.org/com-pkg/com-pkg.de.html
 [12]: http://www.bookzilla.de
 [13]: http://fellowship.fsfe.org/index.de.html
 [14]: http://wiki.fsfe.org/CategoryFellowshipGroup
 [15]: http://www.pdfreaders.org/
 [16]: http://fellowship.fsfe.org/grant.de.html
 [17]: http://www.fsfe.org/contribute/contribute.de.html
 [18]: http://www.fsfe.org/contribute/translators/translators.de.html
 [19]: http://mail.fsfeurope.org/mailman/listinfo/translators
 [20]: http://www.schiessle.org