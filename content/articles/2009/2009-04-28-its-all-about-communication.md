---
title: It’s all about communication
author: Björn Schießle
type: post
date: 2009-04-27T23:43:29+00:00
slug: its-all-about-communication
categories:
  - english
tags:
  - fsfe

---
About one week ago the new [Fellowship][1] web page was launched. It&#8217;s a great improvement over the old one. Finally we have a first-class [blogging][2] platform, a first class [wiki][3] and a [planet][4] to aggregate all weblogs of FSFE&#8217;s Fellows. I think this components already show that communication (blogs, planet) and collaboration (wiki) is an essential part of the Fellowship. Beside increasing FSFE&#8217;s financial independence and political weight the Fellowship always aimed to bring Free Software supporters together and offer them a place to exchange ideas and collaborate on Free Software activities.

But the Fellowship offers even more ways to collaborate and communicate. There are mailing lists for various languages and regional Fellowship groups, there is a Jabber server with multi-user chats (MUC), there are IRC channels and there are even Fellowship meetings for real life contacts. I always thought that this infrastructure was not as visible as it should be. So as one of my contribution to the new web page I helped to create the [communicate][5]-page which gives an overview of all these options.

Just take a look at it. Maybe you will find some mailing lists or chat rooms you are interested in. With this web page in place I hope more Fellows will know an use the various communication channels.

 [1]: http://fellowship.fsfe.org/
 [2]: http://blogs.fsfe.org
 [3]: http://wiki.fsfe.org
 [4]: http://planet.fsfe.org
 [5]: http://fellowship.fsfe.org/communicate.html