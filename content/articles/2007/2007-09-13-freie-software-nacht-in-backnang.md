---
title: Freie Software Nacht in Backnang
author: admin
type: post
date: 2007-09-13T22:41:52+00:00
slug: freie-software-nacht-in-backnang
categories:
  - deutsch
tags:
  - FreeSoftware
  - sfd

---
<img src="/img/articles/sfd2007.png" class="article-image" style="float: right;" />
Der 15. September wird von verschiedenen Gruppierungen international als [&#8220;Software Freedom Day&#8221;][1] ausgerufen. Weltweit finden Veranstaltungen statt, um auf die Vorzüge Freier Software &#8212; welche von jedem Menschen uneingeschränkt benutzt, verändert und weitergegeben werden darf &#8212; aufmerksam zu machen.

Die [Linux User Group Backnang][2] beteiligt sich hieran mit einer &#8220;Freien Software Nacht&#8221; in der Bar [&#8220;Das Wohnzimmer&#8221;][3] in Backnang. An verschiedenen Tischen sollen Projekte vorgestellt werden, an denen sich die Linux User Group beteiligt. Kostenlose GNU/Linux-CDs werden ebenfalls verteilt.

Die Linux User Group wird den gesamten Abend vor Ort für Fragen und Gespräche zur Verfügung stehen.

Wenn Ihr also schon immer auf eine Möglichkeit gewartet habt, um mit anderen Freunden Freier Software und Fellows aus der Region in Kontakt zu kommen, dann könnte das die Gelegenheit sein!
  
Ich werde da sein und würde mich freuen, den einen oder anderen Fellow bei dieser Gelegenheit kennen zu lernen.

 [1]: http://www.softwarefreedomday.org
 [2]: http://www.lug-bk.de
 [3]: http://www.daswohnzimmer.com/
