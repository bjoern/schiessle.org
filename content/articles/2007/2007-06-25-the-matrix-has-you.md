---
title: The Matrix Has You
author: admin
type: post
date: 2007-06-25T10:05:03+00:00
slug: the-matrix-has-you
categories:
  - english
tags:
  - FreeSoftware
  - fun

---
While browsing the Web i have found the video presentation [&#8220;Free Software and the Matrix&#8221;][1] by Alexandre Oliva.

Just take some time, watch this presentation and you will see how much the movie &#8220;The Matrix&#8221; discusses the issues of the Free Software movement.

 [1]: http://www.lsd.ic.unicamp.br/~oliva/fsfla/whatisthematrix/
