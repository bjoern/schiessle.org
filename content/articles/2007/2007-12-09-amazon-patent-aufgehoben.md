---
title: Amazon Patent aufgehoben
author: admin
type: post
date: 2007-12-09T20:27:41+00:00
slug: amazon-patent-aufgehoben
categories:
  - deutsch
tags:
  - swpat

---
Der Förderverein für eine Freie Informationelle Infrastruktur ([FFII][1]) hatte vor dem Europäischen Patentamt (EPA) Einspruch gegen das Patent zur Online-Geschenkbestellung ([EP0927945][2]) von Amazon eingelegt. Das Patent wurde 2003 erteilt. Letztlich beschreibt das Patent eine ganz normale Online Bestellung wie sie in fast jedem Online-Shop durchgeführt wird, mit dem Unterschied, dass der Besteller die Ware als Geschenk an einen anderen Empfänger verschicken lässt.

Reine Software ist nach Artikels 52 des Europäischen Patentübereinkommens (EPÜ) nicht patentierbar. Der Vertreter von Amazon beharrte jedoch darauf, dass es sich hierbei um eine patentfähige Erfindung handelt, da es einen Computer benötigen würde und dadurch eine &#8220;technische Erfindung&#8221; sei. Die Patentprüfer wollten dieser Sichtweise aber nicht folgen und entgegneten &#8220;Die Computer waren schon immer in der Lage, das zu tun. Sie haben sie lediglich dazu programmiert!&#8221;

Einziger Wermutstropfen: Die Patentprüfer konnten sich nicht zu einer grundsätzlichen Ablehnung der Patentfähigkeit durchringe, sondern begründeten ihre Ablehnung mit dem Fehlen eines &#8220;erfinderischen Schritts&#8221;.

Hartmut Pilch (FFII) kommentierte die Entscheidung &#8220;Das ist gekünstelt und zeigt, dass die Schlacht um Softwarepatente noch lange nicht vorbei ist&#8221;, Georg Jakob (FFII) fügte hinzu &#8220;Allerdings wäre das EPA noch vor ein paar Jahren einfach den USA &#8211; und damit Amazon gefolgt. Heute werden aber wenigstens die grundlegenden Probleme deutlicher erkannt. Das ist auch ein deutliches Zeichen dafür, dass unsere Argumentation selbst innerhalb des EPA immer mehr Gehör findet und die bisherige Praxis auf die Dauer nicht zu halten sein wird &#8211; es ist nur eine Frage der Zeit.&#8221;

 [1]: http://www.ffii.de
 [2]: http://v3.espacenet.com/textclam?DB=EPODOC&IDX=EP0927945&CY=ep&QPN=EP0927945