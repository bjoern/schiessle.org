---
title: Quick Guide to GPLv3
author: admin
type: post
date: 2007-11-08T21:30:24+00:00
slug: quick-guide-to-gplv3
categories:
  - english
tags:
  - FreeSoftware
  - gpl

---
The FSF has released a [Quick Guide to GPLv3][1]. This article explains all major changes in an easy-to-understand overview. So it is a good staring point for everyone who wants to understand GPLv3.

It is also a good resource for developers who plan to release their software under the GPLv3.

 [1]: http://www.fsf.org/licensing/licenses/quick-guide-gplv3.html
