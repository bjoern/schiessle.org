---
title: A new toy arrived
author: Björn Schießle
type: post
date: 2010-12-19T10:10:22+00:00
slug: a-new-toy-arrived
categories:
  - english
tags:
  - debian
  - hardware
  - kolab
  - owncloud

---
<img alt="Thin Client" src="/img/articles/qbox270-homeserver.jpg" class="article-image" style="float: right; width: 300px;" />

My little new toy arrived! It&#8217;s a Thin Client Tux@Home Q-Box 270 (Intel Atom) with 1GB RAM and 500GB hard disk. One of the nice things about the device is the low power consumption, only 10-15Watt. This is important because I want to use the device as a small home server. To avoid paying the [&#8220;windows tax / proprietary software tax&#8221;][1] I bought the computer at ixsoft.de, a online shop which sells hardware with [GNU/Linux][2] pre-installed. It came with <a hre="www.fedoraproject.org">Fedora GNU/Linux</a> and now runs the [Debian GNU/Linux (Squeeze)][3] operating system which I consider more suitable for the tasks I want to use the device.

Now the more interesting part: What software / services should run on this little helper? In a first step I want to enable ssh access from outside, install an IRC bouncer and a subversion (SVN) server. The next think I would like to install is some kind of address book and calendar which can be used from any device (desktop computer, laptop, smart phone). But therefor I have to look into some solutions first. [Kolab][4] looks quite interesting. But I have to see how well it works with my software/hardware setup (Claws-Mail (Thunderbird) and Android). [OwnCloud][5] is another project I want to look at. Maybe this could be a nice solution for some file hosting.

If you have a tip for a good address book and calender solution or any other fancy idea what could be done with the Thin Client than drop me a mail or add a comment.

 [1]: http://wiki.fsfe.org/WindowsTaxRefund
 [2]: http://www.gnu.org/gnu/linux-and-gnu.html
 [3]: http:///www.debian.org
 [4]: http://kolab.org/
 [5]: http://owncloud.org/