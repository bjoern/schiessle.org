---
title: Freie Software Unterstützen – “I joined the Game”
author: Björn Schießle
type: post
date: 2010-12-02T23:17:41+00:00
slug: freie-software-unterstutzen-i-joined-the-game
categories:
  - deutsch
tags:
  - kde
  - support
  - FreeSoftware

---
<img src="/img/articles/kde-join-the-game.png" class="article-image" style="float: right; width: 211px" />
[&#8220;Join the Game&#8221;][1] wurde auf dem diesjährigen Linuxtag vorgestellt. Dabei handelt es sich um ein Programm des [KDE e.V.][2], welches es ermöglicht das [KDE Projekt][3] finanziell zu unterstützen.

Als ich davon gelesen hatte musste ich spontan an die FOSDEM 2005 denken. Damals startete die [Free Software Foundation Europe (FSFE)][4] das [Fellowship][5] Programm. Ohne lange darüber nachzudenken wurde ich noch am selben Tag ein Fellow. Ähnlich schnell entschied ich mich dazu bei &#8220;Join the Game&#8221; mitzumachen. Während die FSFE mit ihrer politischen und gesellschaftlichen Arbeit für ein Umfeld und ein Bewusstsein sorgt, in dem [Freie Software][6] entstehen und wachsen kann, deckt KDE den praktischen Teil freier Software ab. Daher war &#8220;Join the Game&#8221; für mich ein logischer Schritt und die perfekte Ergänzung zu meiner mittlerweile langjährigen Unterstützung der FSFE (sowohl finanziell als auch durch ehrenamtliche Mitarbeit).

> &#8220;Sei du selbst die Veränderung, die du dir wünschst für diese Welt&#8221; (Mahatma Gandhi)

Auch du kannst Freie Software unterstützen! Neben dem Fellowship und &#8220;Join the Game&#8221; gibt es selbstverständlich noch viele weitere Möglichkeiten um Freie Software zu fördern (sowohl finanziell als auch durch Mitarbeit). Eine einfache Möglichkeit Freie Software ganz nebenbei zu fördern besteht darin, seine Bücher bei [Bookzilla][7] zu kauft oder im Fall von Amazon dieses [Firefox-Plugin][8] zu verwenden. Damit geht bei jedem Einkauf automatisch eine kleine Provision an die FSFE.<div class="share-on-diaspora" data-url="http://blog.schiessle.org/2010/12/03/freie-software-unterstutzen-i-joined-the-game/" data-title="Freie Software Unterstützen - "I joined the Game"">

 [1]: http://jointhegame.kde.org
 [2]: http://ev.kde.org/
 [3]: www.kde.org
 [4]: www.fsfe.org
 [5]: fellowship.fsfe.org
 [6]: http://fsfe.org/about/basics/freesoftware
 [7]: www.bookzilla.de
 [8]: http://affiliatefox.net/install/install.php?hash=2773e78c1065e77e628741be8c2ab1fa
 [9]: https://blog.schiessle.org/wp-content/plugins/wp-freedom-share/diaspora/img/diaspora-share.png
