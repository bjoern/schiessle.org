---
title: FSFE mit Theodor-Heuss-Medaille ausgezeichnet
author: Björn Schießle
type: post
date: 2010-01-26T16:59:07+00:00
slug: fsfe-mit-theodor-heuss-medaille-ausgezeichnet
categories:
  - deutsch
tags:
  - fsfe
  - stuttgart

---
Die überparteiliche [Theodor Heuss Stiftung][1] wurde 1964 gegründet und trägt den Namen des ersten Bundespräsidenten der Bundesrepublik Deutschland. Die Stiftung vergibt seit ihrer Gründung alljährlich den Theodor Heuss Preis und die dem Preis ebenbürtigen Theodor Heuss Medaillen, um bürgerschaftliche Initiative und Zivilcourage zu fördern. Die Stiftung will damit &#8220;auf etwas hinweisen, was in unserer Demokratie getan und gestaltet werden muss, ohne dass es bereits vollendet ist&#8221; (Carl Friedrich v. Weizsäcker, 1965).

Dieses Jahr wurde die [Free Software Foundation Europe (FSFE)][2] zusammen mit [Oxfam Deutschland][3] mit der Theodor-Heuss-Medaille ausgezeichnet. Die Verleihung wird am 8. Mai in Stuttgart stattfinden.

Die Theodor Heuss Stiftung schreibt hierzu:

> Die Theodor Heuss Medaillen 2010 gehen an die Free Software Foundation Europe e. V. (Düsseldorf) und an Oxfam Deutschland e.V. (Berlin), die sich durch neue Strategien der kooperativen Wertschöpfung und durch verantwortungsvolles Handeln für eine gerechtere Welt einsetzen.

In der [Pressemitteilung der FSFE][4] sagt Karsten Gerloff: &#8220;Freie Software ist die unverzichtbare Komponente einer freiheitlichen Gesellschaft im digitalen Zeitalter. Sie sichert den gleichberechtigten Zugang aller Menschen zur Informationsgesellschaft&#8221; und zeigt sich hoch erfreut über die Auszeichnung. &#8220;Die Free Software Foundation Europe wurde im November 2000 von einer kleinen Gruppe von Menschen ins Leben gerufen, die ihrer Zeit voraus waren und unermüdlich und unter großem persönlichen Einsatz für ihren Erfolg gearbeitet haben. Insbesondere zu nennen sind hier der Initiator der FSFE, Georg Greve, der die FSFE unter erheblichem persönlichen Risiko gegründet und bis zum Juni 2009 aufgebaut und geleitet hat, sowie Mitgründer Bernhard Reiter, der das deutsche Team über lange Jahre zu einer der stärksten Gruppen für Freie Software in Europa gemacht hat.&#8221;, so der Präsident der FSFE.

Ich selbst bin mittlerweile seit 2005 ehrenamtlich für die FSFE aktiv. Angefangen als [Fellow][5] und Übersetzer bis hin zur Mitgliedschaft im deutschen Team und seit einem guten Jahr im europäischen Kernteam. An Tagen wie diesen freue ich mich besonders über die Anerkennung, welche die FSFE mittlerweile aus vielen verschiedenen Richtungen erfährt. Dies alles wäre ohne den vielen [Menschen][6], [Fellows][7], [Unterstützer][8] und [Spender][9], welche hinter der FSFE stehen nicht möglich.

Auch das Web- und Übersetzer-Team soll an dieser Stelle nicht unerwähnt bleiben, welches in den letzten Tagen hart gearbeitet hat um die Webseiten zu aktualisieren und die Pressemitteilung rechtzeitig zur Bekanntgabe der Auzeichnung in viele Sprachen übersetzt hat.

 [1]: http://www.theodor-heuss-stiftung.de
 [2]: http://www.fsfe.org
 [3]: http://www.oxfam.de
 [4]: http://fsfe.org/news/2010/news-20100126-01.de.html
 [5]: http://wiki.fsfe.org/Fellows/schiessle
 [6]: http://fsfe.org/about/team.de.html
 [7]: http://fellowship.fsfe.org
 [8]: http://fsfe.org/contribute/contribute.de.html
 [9]: http://fsfe.org/donate/thankgnus.de.html