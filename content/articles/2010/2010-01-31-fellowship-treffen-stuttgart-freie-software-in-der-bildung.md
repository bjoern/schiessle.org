---
title: Fellowship Treffen Stuttgart – Freie Software in der Bildung
author: Björn Schießle
type: post
date: 2010-01-31T12:47:01+00:00
slug: fellowship-treffen-stuttgart-freie-software-in-der-bildung
categories:
  - deutsch
tags:
  - education
  - fsfe
  - stuttgart

---
In den letzten Monaten gab es leider keine Treffen der [Fellowship-Gruppe Stuttgart][1]. Das wollen wir im Jahr 2010 wieder ändern! Daher möchte wir euch am Donnerstag, den 11. Februar zum Fellowship-Treffen in Stuttgart einladen. Wir werden uns in [Sophies Brauhaus][2] ab 20 Uhr treffen.

An diesem Tag wird auch Thomas Jensch, Koordinator des [Edu-Teams][3] der [FSFE][4], anwesend sein. Thomas wird über den aktuellen Stand des Edu-Teams, geplante Aktivitäten und allgemein über [Freie Software][5] in der Bildung berichten. Wenn Ihr euch für das Thema interessiert oder schon selber Erfahrung mit Freier Software in der Bildung gesammelt habt, dann ist dieses Treffen genau das richtige für euch. Gerne dürft Ihr auch Freunde und Bekannte mitbringen die sich allgemein für Freie Software oder gezielt für Freie Software in der Bildung interessieren.

Wenn darüber hinaus noch etwas Zeit bleibt, dann würde ich die Gelegenheit gerne nutzen um mit euch über den Neustart eines regelmäßigen [Fellowship][6]-Treffen in Stuttgart zu sprechen. Was sind eure Interessen? Was erwartet bzw. erhofft Ihr euch von einem regelmäßigen Fellowship-Treffen in Stuttgart? usw.

Thomas und ich freuen uns schon darauf euch am Donnerstag, den 11. Februar in Stuttgart zu treffen.

Um einen ausreichend großen Tisch reservieren zu können, würde ich mich über eine kurze Rückmeldung freuen!

 [1]: http://wiki.fsfe.org/groups/Stuttgart
 [2]: http://www.sophies-brauhaus.de
 [3]: http://fsfe.org/projects/education
 [4]: http://fsfe.org
 [5]: http://fsfe.org/about/basics/freesoftware
 [6]: http://fellowship.fsfe.org