---
title: DRM – Digitale Rechteminderung
author: Björn Schießle
type: post
date: 2010-10-03T19:24:43+00:00
slug: drm-digitale-rechteminderung
categories:
  - deutsch
tags:
  - drm

---
DRM beschreibt ein System, welches entwickelt wurde um die Verwendung digitaler Werke zu kontrollieren und zu überwachen. Typische DRM-Systeme werden auf Textdokumente, Musik und andere Medien angewandt. Mit DRM entscheidet der Computer bzw. der Programmierer der Software darüber, ob du einen Text lesen, drucken oder kopieren kannst, ob du ein Musikstück anhören kannst und wenn ja mit welchen Geräten oder in welchen Ländern du deine gekaufte DVD abspielen kannst.   
Hierdurch wird dem Anwender die Kontrolle über seine Daten und seine Geräte entzogen. Diese Entwicklung ist auch mit Blick auf unsere Demokratie bedenklich, da DRM nicht nur den Anwender bevormundet sondern auch die staatliche Regulierung unterläuft. Werden solche Systeme eingesetzt, dann entscheidet nicht mehr der Staat über erlaubte und regulierte Handlungen sondern die Produzenten der Medien und der Geräte entscheiden in letzter Instanz welche Rechte der Anwender hat.

Neben diesen klassischen Einsatzfeldern von DRM werden solche Systeme heute auch eingesetzt um sicherzustellen, dass ein Gerät nur Software ausführt welche vom Hersteller signiert wurde. Diese Art von DRM ist auch unter dem Namen [Tivoisierung][1] bekannt, angelehnt an eine Firma welche digitale Videorecorder mit dieser Art von DRM ausrüstet. Hierdurch ist es zum Beispiel möglich dem Anwender die Freiheiten, welche ihm [Freie Software][2] bietet, zu verwehren.

Befürworter solcher Systeme möchten, dass wir DRM als &#8220;digitales Rechtemanagement&#8221; verstehen. In der Realität werden solche Systeme aber dazu verwenden um die Rechte der Anwender einzuschränken, oft über die eigentlichen Grenzen des Urheberrechts hinaus. Daher sprechen viele Leute im englischen gerne von &#8220;Digital Restrictions Management&#8221;. Die deutsche Übersetzung hierzu &#8220;digitales Restriktionsmanagement&#8221; war für mich sprachlich nie eine optimale Lösung. Durch das Editorial &#8220;Verlesene Rechte&#8221; der Zeitschrift iX bin ich auf den Begriff &#8220;digitale Rechteminderung&#8221; aufmerksam geworden, welcher wohl schon vor ein paar Jahren durch die Frankfurter Allgemeine geprägt wurde. Digitale Rechteminderung halte ich sowohl sprachlich als auch inhaltlich für eine treffende Bezeichnung von DRM-Systemen und werde diese in Zukunft verwenden.

 [1]: http://de.wikipedia.org/wiki/Tivoisierung
 [2]: http://fsfe.org/about/basics/freesoftware.de.html