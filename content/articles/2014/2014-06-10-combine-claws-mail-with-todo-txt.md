---
title: Integrate ToDo.txt into Claws Mail
author: Björn Schießle
type: post
date: 2014-06-10T19:22:56+00:00
slug: combine-claws-mail-with-todo-txt
categories:
  - english
tags:
  - claws
  - hacking
  - todo.txt

---
I use [Claws Mail][1] for many years now. I like to call it &#8220;the mutt mail client for people who prefer a graphical user interface&#8221;. Like Mutt, Claws is really powerful and allows you to adjust it exactly to your needs. During the last year I began to enjoy managing my open tasks with [ToDo.txt][2]. A powerful but still simple way to manage your tasks based on text files. This allows me not only to manage my tasks on my computer but also to keep it in sync with my mobile devices. But there is one thing I always missed. Often a task starts with an email conversation and I always wanted to be able to transfer a mail easily to as task in a way, that the task links back to the original mail conversation. Finally I found some time to make it happen and this is the result:

To integrate ToDo.txt into Claws-Mail I wrote the Python program [mail2todotxt.py][3]. You need to pass the path to the mail you want to add as parameter. By default the program will create a ToDo.txt task which looks like this:

`<task_creation_date> <subject_of_the_mail> <link_to_the_mail>`

Additionally you can call the program with the parameter _&#8220;-i&#8221;_ to switch to the interactive mode. Now the program will ask you for a task description and will use the provided description instead of the mail subject. If you don&#8217;t enter a subscription the program will fall back to the mail subject as task description. To use the interactive mode you need to install the Gtk3 Python bindings.

To call this program directly from Claws Mail you need to go to Configuration->Actions and create a action to execute following command:

`/path_to_mail2todotxt/mail2todotxt.py -i %f &`

Just skip the _-i_ parameter if you always want to use the subject as task description. Now you can execute the program for the selected mail by calling Tools->Actions->"The-name-you-choose-for-the-action". Additional you can add a short-cut if you wish, e.g. I use &#8220;Ctrl-t&#8221; to create a new task.

Now that I&#8217;m able to transfer a mail to a ToDo.txt item I also want to go back to the mail while looking at my open tasks. Therefore I use the &#8220;open&#8221; action from Sebastian Heinlein which I extended with an handler to open claws mail links. After you added this action to your _~/.todo.action.d_ you can start Claws-Mail and jump directly to the referred mail by typing:

`t open <task_number_which_referes_to_a_mail>`

The original version of the &#8220;open&#8221; action can be found at [Gitorious][4]. The modified version you need to open the Claws-Mail links can be found [here][5].

 [1]: http://claws-mail.org/
 [2]: http://todotxt.com/
 [3]: https://github.com/schiesbn/mail2todotxt
 [4]: https://gitorious.org/todo-txt-glatzor
 [5]: https://github.com/schiesbn/todotxt