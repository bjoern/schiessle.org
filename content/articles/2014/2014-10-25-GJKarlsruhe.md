---
title: Freie Software in Politik und Gesellschaft bei der Grünen Jugend Baden-Württemberg
author: Björn Schießle
type: post
date: 2014-10-25T19:22:56+00:00
categories:
  - deutsch
tags:
  - grüne
  - policy
  - FreeSoftware
  - fsfe
  - slides
slides: true
image: articles/slides/GJKarlsruhe14/slide1.jpg
summary: Workshop bei der Grünen Jugend Baden-Württember zur politischen und gesellschaftlichen Bedeutung Freier Software.
location: Karlsruhe, Germany
headerImage: fsfs.jpg
---
Ich war heute bei der Grünen Jugend Baden Württemberg zu einem Workshop in Karlsruhe
eingeladen. Thema des Workshops war die politische und gesellschaftliche Bedeutung
Freier Software. Der Workshop wurde sehr gut angenommen und es ergaben sich
anschließend noch viele interessante Diskussionen. Hier sind die Folien meiner
Präsentation:

<div id="workshop-slides" class="slides">
<ul>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide1.jpg" alt="˘ ˇˆ˙ ˝ˇˇ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide2.jpg" alt="˚ !" ˇˆ˙ˆ# ˙˜˙$%%& ˝ˇˇ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide3.jpg" alt="*$%%+ $%+% ,-.- 0ˇˆ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide4.jpg" alt="" /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide5.jpg" alt="1ˇ 2 3645 738 ,˜ 9632 : -! ˛˛˛ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide6.jpg" alt="1ˇ ; => " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide7.jpg" alt="7 7 1 " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide8.jpg" alt="? ˇ %+%++%++%+%+%+%+ %+%%+%+%+%++%+%% %+++++%%%%++%+%+ %%%+%+%+%+%+%+++ +%+%+%+%+%+%+%%% %++++%%+%+%+%+%+ %%+%+%+%+%%%+%+% +%+++%+%+%+%++%+ %+%++%%+%+%+%+%+ %+%%%%%+ AB˛C DEF ˇDG.>;EH I " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide9.jpg" alt="J 1#ˆ8 !1& " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide10.jpg" alt="ˇˆ 3ˇˆL M2 !ˇˆD! - ˜Nˇ( ˇ DˇˆE 3 D˜ˇE " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide11.jpg" alt="4ˇ ! #ˆ ˆ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide12.jpg" alt=")- ˇ 3) D0 #E ˙# D=   #E 'Mˇ D˙#  E " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide13.jpg" alt="# 8Qˇ (O)˛˛R++ ˆ( )˛˛'3'=3 #(D)˛˛9'=3E ˜ ˆ #˜ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide14.jpg" alt="1ˇ = ˇˆ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide15.jpg" alt="1ˇ = 8Sˆ* G O +&%?#< " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide16.jpg" alt="˙6ˇ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide17.jpg" alt="9˜# " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide18.jpg" alt=", " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide19.jpg" alt="7˜# " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide20.jpg" alt="˜M " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide21.jpg" alt="ˇ 7 : #1ˇ - 1@ # :: 0 - 0˜():˜ - 0ˇ)ˆ( " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide22.jpg" alt="ˇ 8 30 0# @7 ˙˚ ˆ˛!˛  "#%˛˛  ˆˆ "# " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide23.jpg" alt="ˇˇ ˇˆ# : 00 1˜ # " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide24.jpg" alt="ˇN 8'%%%T .ˇ 2$%%&*V˛%%%W˛%%% X) "@ $% " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide25.jpg" alt="?  ˇ ˇˆ 7ˆO O1 ˇˆ(˜ ˇˆ ˙ ˇˆN11 'ˇ " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide26.jpg" alt="!#ˇ  # " /></li>
<li><img src="/img/articles/slides/GJKarlsruhe14/slide27.jpg" alt="0 ˙ &˛ˆ"#' ˝ˇ ˝ *˛ ( ˆˆˆ˛˛  " /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

