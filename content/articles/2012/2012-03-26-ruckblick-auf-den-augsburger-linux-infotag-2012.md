---
title: Rückblick auf den Augsburger Linux-Infotag 2012
author: Björn Schießle
type: post
date: 2012-03-26T17:54:33+00:00
slug: ruckblick-auf-den-augsburger-linux-infotag-2012
categories:
  - deutsch
tags:
  - augsburg
  - FreeSoftware
  - fsfe

---
<img src="/img/articles/augsburg-linux-info-tag-2012-1.jpg" class="article-image" style="float: right; width: 300px" />
Letztes Wochenende war es wieder so weit, der [&#8220;Augsburger Linux-Infotag&#8221;][2] stand vor der Tür. Dank der Organisation von Rolf und Wolfgang war die [FSFE][3] auch dieses Jahr wieder mit einem Stand vertreten. Auch durch die strategisch gute Position unseres Standes, ungefähr in der Mitte des Gangs direkt gegenüber den Räumen in denen die Vorträge statt fanden,  konnten wir uns über zahlreichen Besuch an unserem Stand zwischen den Vorträgen freuen.

<img src="/img/articles/augsburg-linux-info-tag-2012-2.jpg" class="article-image" style="float: left; width: 300px" />
Wie sich bereits in den letzten Wochen abzeichnete, war das Thema <a title="Free Your Android" href="http://www.FreeYourAndroid.org" target="_blank">&#8220;Free Your </a><a title="Free Your Android" href="http://www.FreeYourAndroid.org" target="_blank">Android&#8221;</a> eines der meist diskutierten und gefragten Themen an unserem Stand. Viele Besucher waren daran interessiert, was sich hinter dem Slogan verbirgt, wie sie ihr Android befreien können bzw. auf was sie beim Kauf eines neuen Handys achten sollten um möglichst problemlos ein alternatives Android-System installieren zu können.

<img src="/img/articles/augsburg-linux-info-tag-2012-3.jpg" class="article-image" style="float: right; width: 300px" />
Neben dem Stand habe ich dieses Jahr einen Vortrag über die bevorstehenden Herausforderungen für die Freie Software Bewegung gehalten. Ziel des Vortrags war es neue Entwicklungen aufzuzeigen und Denk- bzw. Diskussionsanstöße zu geben, wie wir als Freie Software Gemeinschaft auf diese Herausforderungen reagieren können. Die Themen des Vortrags reichten von [RestrictedBoot][6] und der Frage &#8220;Wer entscheidet in Zukunft darüber, was wir auf unseren Computer installieren können?&#8221; über Cloud Computing und Verteilte soziale Netzwerke bis hin zu der Frage, wie wir mit der <img src="/img/articles/augsburg-linux-info-tag-2012-4.jpg" class="article-image" style="float: left; width: 300px" /> Herausforderungen umgehen, vor die uns neue Geräte wie Tablets und Smartphones stellen, die immer mehr den klassischen PC ablösen. Von den Tablets und Smartphones habe ich dann noch den Bogen zu Freier Software, eBooks und Tablets in der Schule gespannt und abschließend noch das Thema [Offene Standards][8] angesprochen, was so kurz vor den <a title="Document Freedom Day" href="http://www.documentfreedom.org" target="_blank">Document Freedom Day</a> natürlich nicht unerwähnt bleiben konnte. Der Vortrag wurde mit großem Interesse aufgenommen. Die Themen [Freie Software in Schulen][9] und [verteilten sozialen Netzwerke][10] haben am meisten Zustimmung gefunden und für reichlich Diskussionsstoff im Anschluss an den Vortrag gesorgt.

Insgesamt war der Augsburger Linux-Infotag eine schöne Veranstaltung mit vielen interessanten Diskussionen, sowohl während meines Vortrags als auch an unserem Info-Stand. Für mehr Eindrücke von der Veranstaltung findet man <a title="Weitere Bilder von Augsburger Linux-Infotag" href="http://www.bildereintopf.de/index.php?r=gallery/picture&id=195217#picture" target="_blank">hier</a> weitere Bilder.

 [2]: http://www.luga.de/Aktionen/LIT-2012/
 [3]: http://www.fsfe.org
 [6]: https://www.fsf.org/campaigns/secure-boot-vs-restricted-boot/statement
 [7]: http://blog.schiessle.org/wp-content/uploads/Linux_Info_Tag_2012_Sonstige_Augsburg_24-03-2012_195260.jpg
 [8]: https://fsfe.org/projects/os/os.html
 [9]: https://fsfe.org/projects/education/education.html
 [10]: http://www.socialswarm.org
