---
title: 'Freie Software: Überleben im Mainstream'
author: Björn Schießle
type: post
date: 2008-09-20T12:44:22+00:00
slug: freie-software-uberleben-im-mainstream
categories:
  - deutsch
tags:
  - FreeSoftware
  - fsfe

---
Shane Coughlan, Koordinator der Freedom Task Force ([FTF][1]) der [FSFE][2], wurde von der [Wiener Fellowship-Gruppe][3] eingeladen, um über die Professionalisierung  Freier Software zu referieren. Diese gelegenheit nutze das ORF für ein ausführliches [Interview][4]:

> In einer Zeit, in der selbst Microsoft das Hohe Lied von Open Source anstimmt, scheint es der freien Software an Gegnern und auch an Motivation zu fehlen. ORF.at sprach mit dem FSFE-Rechtsexperten Shane Martin Coughlan über Microsoft, Google Chrome und die neuen Herausforderungen für freie Entwickler.

 [1]: http://www.fsfeurope.org/ftf/
 [2]: http://www.fsfeurope.org
 [3]: https://wiki.fsfe.org/FellowshipGroup/Vienna
 [4]: http://futurezone.orf.at/hardcore/stories/305512/
