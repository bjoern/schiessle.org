---
title: Erstes Fellowship Treffen in Stuttgart
author: admin
type: post
date: 2008-08-18T12:50:23+00:00
slug: erstes-fellowship-treffen-in-stuttgart
categories:
  - deutsch
tags:
  - fsfe
  - stuttgart

---
Am Freitag, den 22. August wird um 19:00 Uhr das erste [Fellowship Treffen in Stuttgart][1] stattfinden. Veranstaltungsort wird das Unithekle/Unitop an der Universität Stuttgart (Campus Vaihingen) sein. Einen genauen Lageplan findet ihr [hier][2].

Das Fellowship-Treffen soll uns die Möglichkeit geben sich in gemütlicher Runde kennen zu lernen. Bei Interesse kann man sich dann überlegen, wie ein regelmäßiges Fellowship-Treffen aussehen könnte.

Eingeladen sind neben unseren Fellows alle, die sich für [Freie Software][3] und die [Arbeit der FSFE][4] interessieren.

Ich freue mich darauf möglichst viele Fellows und Freie Software Interessierte aus der Region nächsten Freitag kennen zu lernen!

 [1]: http://wiki.fsfe.org/FellowshipGroup/Stuttgart
 [2]: http://www.lug-s.org/unitop_/
 [3]: http://www.fsfeurope.org/documents/freesoftware.de.html
 [4]: http://www.fsfeurope.org/projects/projects.de.html