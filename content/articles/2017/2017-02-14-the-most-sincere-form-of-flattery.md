---
title: The most sincere form of flattery
author: Björn Schießle
type: post
date: 2017-02-14T10:51:31+00:00
slug: the-most-sincere-form-of-flattery
categories:
  - english
tags:
  - agpl
  - licenses
  - nextcloud
headerImage: articles/looking-for-freedom.jpg
headerImageCopyright: By Daniel Lee (CC BY-ND 2.0)
---

[Nextcloud][1] now exists for almost exactly 8 months. During this time we put a lot of efforts in polishing existing features and developing new functionality which is crucial to the success of our users and customers.

As promised, everything we do is [Free Software][2] (also called Open Source), licensed under the terms of the [GNU APGLv3][3]. This gives our users and customers the most possible flexibility and independence. The ability to use, study, share and improve the software also allows to integrate our software in other cloud solutions as long as you respect the license and we are happy to see that people make use of this rights actively.

## Code appearing in other app stores

We are proud to see that the quality of our software is not only acknowledged by our own users but also by users of other cloud solutions. Recently more and more of our applications show up at the ownCloud App Store. For example the community driven [News app][4] or the [Server Info app][5], developed by the Nextcloud GmbH. Additionally we have heard that our [SAML authentication application][6] is widely considered far better quality than other, even proprietary alternatives, and used by customers of our competitors in especially the educational market. All this is completely fine as long as the combination of both, our application and the rest of the server, is licensed under the terms of the GNU AGPLv3.

## Not suitable for mixing with enterprise versions

While we can&#8217;t actively work on keeping our applications compatible with other cloud solutions, we welcome every 3rd party efforts on it. The only draw-back, most of the other cloud solutions out there make a distinction between home users and enterprises on a license level. While home users get the software under a Free Software license, compatible with the license of our applications, Enterprise customers don&#8217;t get the same freedom and independence and are therefore not compatible with the license we have chosen. This means that all the users who uses propriety cloud solutions (often hidden by the term &#8220;Enterprise version&#8221;) are not able to legally use our applications. We feel sorry for them, but of course a solution exists &#8211; get support from the people who wrote your software rather than a different company. In general, we would recommend buying support for real Free Software and not just Open Source marketing.

Of course we don&#8217;t want to sue people for copyright violation. But Frank choose the AGPL license 7 years ago on purpose and we want to make sure that the users of our software understand the license and it&#8217;s implications. In a nutshell, the GNU AGPLv3 gives you the right to do with the software whatever your want and most important all the entrepreneurial freedom and independence your business needs, as long as the combined work is again licensed under the GNU AGPLv3. By combining GNU AGPLv3 applications with a proprietary server, you violate this rule and thus the terms of the license. I hope that other cloud solutions are aware of this problem, created by their open-core business model, and take some extra steps to protect their customers from violating the license of other companies and individual contributors. For example by performing a license check before a application gets enabled.

## Open Core is a bad model for software development

This is one of many problems arising from the usage of open core business models. It puts users on risk if they combine the proprietary part with Free Software, more about it can be read [here][7]. That&#8217;s why we recommend all enterprise and home users to avoid a situation where proprietary and free licensed software is combined. This is a legal minefield. We at Nextcloud decided to take a clear stance on it. Everything is Free Software and there is only one version of the software for both home users and enterprises. Thus allows every home user, customer or partner to use all applications available as long as they respect the license.

 [1]: https://nextcloud.com
 [2]: https://fsfe.org/about/basics/freesoftware.en.html
 [3]: https://www.gnu.org/licenses/agpl-3.0
 [4]: https://apps.owncloud.com/content/show.php/News?content=174796
 [5]: https://apps.owncloud.com/content/show.php/Server+info?content=174795
 [6]: https://github.com/nextcloud/user_saml
 [7]: https://nextcloud.com/blog/8-ways-businesses-benefit-from-purchasing-open-source/
