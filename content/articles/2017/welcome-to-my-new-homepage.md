---
date: 2017-07-09T11:04:22+02:00
categories:
 - english
type: post
tags:
 - homepage
 - hugo
 - gitlab
 - rsync
title: Welcome to my new Homepage
friendica: https://soc.schiessle.org/display/bjoern/5275
---

Finally I moved my homepage a a complete static page powered by Hugo. 
Here I want to document some challenges I faced during the transition
and how I solved them.

## Basic setup

As already said I use Hugo to generate the static sites. My theme is
based on [Sustain](https://github.com/nurlansu/hugo-sustain).
I did some changes and uploaded my version to
[GitLab](https://gitlab.com/BeS/hugo-sustain-ng).

I want to have all dependencies like fonts and JavaScript libraries 
locally, so this was one of the largest changes to the original theme.
Further I added a easy way to add some share buttons to a blog post,
like you can see at the end of this article. The theme also contains
a nice and easy way to add presentations or general slide shows to 
the webpage, some examples can be seen [here](/tags/slides/). 
The theme contains a example site which shows all this features.

## Comments

This was one of the biggest challenges. I had some quite good
discussion on my old blog powered by Wordpress so I don't want to lose
this feature completely. There are some solutions for static pages but
non of these are satisfying. For example [Staticman](https://staticman.net/)
looks really promising. Sadly it only works with GitHub. Please let me
know if you know something similar which doesn't depend on GitHub.

For now I decided to do two things. By default I add a short text at
the end of each article to tell people to send me a e-mail if they
want to share or discuss their view on the topic. Additionally I can
add to the meta data of each posts a link to a Friendica
post. In this case the link will be added at the end of the article,
inviting people to discuss the topic on this free, decentralised and
federated network. I have chosen Friendica because it allows users to
interact with my blog posts not only with a Friendica
account but also with a Diaspora, GNU Social, Mastodon or Hubzilla
account. If you have a account on one of these networks and want to get
updates about new blog posts in order to participate in conversations around it, follow
<a href="https://soc.schiessle.org/profile/blog/?tab=profile">this
Friendica account</a>. I also created a [more detailed description](/fediverse)
for people new to the world of free social networking.

## Deployment

After all the questions above where answered and a first version of the new
webpage was in place, I had to find a easy way to deploy it. I host the
source code of my homepage on GitLab which has a nicely integrated CI
service which can be used to deploy the webpage on any server.

Therefore we need to add a CI script called `.gitlab-ci.yml` to the
root of the repository. This script needs to contain following (please 
adjust the paths):

````
image: publysher/hugo

before_script:
  - apt-get update
  - apt-get --yes --force-yes install git ssh rsync
  - git submodule update --init --recursive

pages:
  script:
  - hugo
  - mkdir "${HOME}/.ssh"
  - echo "${SSH_HOST_KEY}" > "${HOME}/.ssh/known_hosts"
  - echo "${SSH_PRIVATE_KEY}" > "${HOME}/.ssh/id_rsa"
  - chmod 700 "${HOME}/.ssh/id_rsa"
  - rsync -hrvz --delete --exclude=_ public/ schiesbn@schiessle.org:/home/schiesbn/websites/schiessle.org/htdocs/
  artifacts:
    paths:
    - public
  only:
  - master
````

We need to create a ssh key-pair to deploy the webpage. For security
reasons it is highly recommend to create a ssh key used only for the
deployment.

The variables `SSH_HOST_KEY` and `SSH_PRIVATE_KEY` need to be set at
GitLab in the CI settings. `SSH_PRIVATE_KEY` contains the private ssh
key which is located in the ~/.ssh directory.

To get the right value for `SSH_HOST_KEY`, we run `ssh-keyscan
<our-webpage-host>`. Once we executed that command, we should
see something similar to `schiessle.org ssh-rsa
AAAAB3NzaC1yc2EAAAADAQABAAACAQCtwsSpeNV...`.  Let's copy this to the
`SSH_HOST_KEY` value in our GitLab settings.

Finally we need to copy the public ssh key to the .ssh/authorized_keys
file on the web-server to allow GitLab to access it.

Now we are already done. The next time we push some changes to the
Github repository GitLab will build the page and sync it to the
web-server.

Using the private key stored in the GitLab settings allows everyone
with access to the key to login to our web-server. Something we don't
want. Therefore I recommend to limit the ssh key to only this one
rsync command from the .gitlab-ci.yml file. In order to do this, we
need to find the exact command send to the web-server by adding
`-e'ssh -v'` to the rsync command.

Executing the rsync command with the additional option should result
in something like:

````
debug1: Sending command: rsync --server -vrze.iLsfxC --delete . /home/schiesbn/websites/schiessle.org/htdocs/
````

we copy this command to create following .ssh/authorized_keys entry:

````
command="rsync --server -vrze.iLsfxC --delete . /home/schiesbn/websites/schiessle.org/htdocs/",no-pty,no-agent-forwarding,no-port-forwarding,no-X11-forwarding ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7Sf/PDty0d0SQPg9b+Duc18RxPGaBKMzlKR0t1Jz+0eMhTkXRDlBMrrkMIdXJFfJTcofh2nyklp9akUnKA4mRBVH6yWHI+j0aDIf5sSC5/iHutXyGZrLih/oMJdkPbzN2+6fs2iTQfI/Y6fYjbMZ+drmTVnQFxWN8D+9qTl49BmfZk6yA1Q2ECIljpcPTld7206+uaLyLDjtYgm90cSivrBTsC4jlkkwwYnCZo+mYK4mwI3On1thV96AYDgnOqCn3Ay9xiemp7jYmMT99JhKISSS2WNQt2p4fVxwJIa6gWuZsgvquP10688aN3a222EfMe25RN+x0+RoRpSW3zdBd
````

Now it is no longer possible to use the private key, stored at GitLab
to login to the web-server or to perform any other command than this
specific rsync command.

## Interesting observation

I run this static webpage now for a few weeks. During this weeks I got quite some
email from people interested in some topic I write about in my blog.
This are not new blog articles, but posts which where already online for
quite some time. Somehow it looks like more people find this articles
after the transition to a static site. Maybe search engines rate the 
static site higher than the old Wordpress page? I don't know, maybe it is
just a coincidence... but interesting.


