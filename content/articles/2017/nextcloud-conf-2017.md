+++
date = "2017-09-25T15:44:52+02:00"
categories = ["english"]
type = "post"
tags = ["FreeSoftware", "licenses", "agplv3", "nextcloud", "slides"]
image = "articles/slides/ncconf2017-licenses/slide1.jpg"
title = "Nextcloud Conference 2017: Free Software licenses in a Nutshell"
summary = "Lightening talk about Free Software licensing and how it is handled at Nextcloud."
location = "Nextcloud Conference 2017 in Berlin, Germany"
slides = "true"
headerImage = "nextcloud.png"
+++

At this years Nextcloud conference I gave a lightening talk about Free Software licenses. Free Software developers often like to ignore the legal aspects of their project, still I think it is important to know at least some basics. The license you chose and other legal decisions you make are a important cornerstone to define the basic rules of the community around your code. Making good choices can enable a level playing field for a large, diverse and growing community.

Explaining this huge topic in just five minutes was a tough challenge. The goal was to explain why we are doing things the way we are doing it. For example why we introduced the [Developer Certificate of Origin](https://github.com/nextcloud/server/blob/master/contribute/developer-certificate-of-origin), a tool to create legal certainty, used by many large Free Software initiatives such as Linux, Docker or Eclipse these days. Further the goal was to transfer some knowledge about license compatibility and give some useful pointers for app developers how to decide whether a third party license is compatible or not. If the five minute lightening talk was to fast (and yes, I talked quite fast to match the time limit) or if you couldn't attend, here are the slides to reread it:

<div id="ncconf2017-licenses"  class="slides">
<ul>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide1.jpg" alt="slide0" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide2.jpg" alt="slide1" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide3.jpg" alt="slide2" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide4.jpg" alt="slide3" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide5.jpg" alt="slide4" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide6.jpg" alt="slide5" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide7.jpg" alt="slide6" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide8.jpg" alt="slide7" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide9.jpg" alt="slide8" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide10.jpg" alt="slide9" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide11.jpg" alt="slide10" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide12.jpg" alt="slide11" /></li>
<li><img src="/img/articles/slides/ncconf2017-licenses/slide13.jpg" alt="slide12" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

