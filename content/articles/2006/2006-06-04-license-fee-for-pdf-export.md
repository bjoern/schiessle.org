---
title: License fee for PDF export?
author: admin
type: post
date: 2006-06-04T22:19:02+00:00
slug: license-fee-for-pdf-export
categories:
  - english
tags:
  - FreeSoftware
  - open standards
  - pdf

---
As a GNU/Linux user i&#8217;m used to have PDF export in almost every program. With Office 2007 Microsoft finally wants to offer this common feature to their users too. But it seems like Adobe doesn&#8217;t like this idea. As cnet [reported][1] Adobe asked Microsoft to remove the PDF export feature or pay a fee for it.
  
Brian Jones from Microsoft has published some [information][2], too.

Adobe promotes PDF as an [open standard][3]. But if Adobe now starts to sue competitors they can no longer claim that PDF is an open standard.

What does this mean for all the Free Software applications with PDF export and for the exchange of documents in general? If Adobe starts to sue Microsoft, who will be the next victim? It would be a big loss for everyone if we can no loger rely on PDF as an open standard.

 [1]: http://news.com.com/Report+Microsoft+expects+Adobe+to+file+antitrust+suit/2100-1012_3-6079320.html?tag=nefd.top
 [2]: http://blogs.msdn.com/brian_jones/archive/2006/06/03/616022.aspx
 [3]: http://en.wikipedia.org/wiki/Open_standard
