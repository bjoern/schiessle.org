---
title: French “iPod Law” violates Human Rights
author: admin
type: post
date: 2006-08-02T21:26:51+00:00
slug: french-ipod-law-violates-human-rights
categories:
  - english
tags:
  - copyright
  - drm

---
The so-called &#8220;iPod law&#8221; contains reduced fines for file sharing and forced companies to open their DRM specification to enable competition.

The French Constitutional Council has declared this aspects unconstitutional. The justification: The &#8220;iPod law&#8221; violated the Human Right of constitutional protections of property.

Mr. Menard, a partner at the Lovells law firm and a specialist in &#8220;intellectual property&#8221; said: &#8220;The Constitutional Council effectively highlighted the importance of intellectual property rights,&#8221; and added that Apple Computer and other companies could not be forced to share their copy-protection technology without being paid for it.

Does it sound like a joke? But it&#8217;s true. For more information read:

&#8211; [New York Times: Parts of French &#8216;iPod Law&#8217; Struck Down (English)][1]
  
&#8211; [Golem: iTunes-Gesetz verstößt gegen Menschenrechte (German)][2]

UPDATE: Jacques Chirac, president of France, has signed the law and so it become valid. I don&#8217;t know if the French Constitutional Council will stop the law but as long as nothing happens the law is legally valid. Great Britain, Sweden, Denmark, Norway and Poland could be the next countries with similar laws. Source (German): [The Inquirer DE][3].<div class="share-on-diaspora" data-url="http://blog.schiessle.org/2006/08/02/french-ipod-law-violates-human-rights/" data-title="French "iPod Law" violates Human Rights">

 [1]: http://www.nytimes.com/2006/07/28/business/28cnd-music.html?ex=1311739200&en=a4590cf789d399fb&ei=5088
 [2]: http://www.golem.de/0608/46891.html
 [3]: http://de.theinquirer.net/2006/08/04/die_lex_itunes_tritt_in_kraft.html
