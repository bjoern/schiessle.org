---
title: Interview with DefectiveByDesign
author: admin
type: post
date: 2006-08-02T15:53:01+00:00
slug: interview-with-defectivebydesign
categories:
  - english
tags:
  - drm

---
Thanks to Markus from [netzpolitik.org][1] i have found this interesting [interview][2] with the [DefectiveByDesign][3] campaign.

The campaign has received quite a lot of attention in the media. For example the [&#8220;Bono petition&#8221;][4] saw press coverage in more than 115 news papers and news sitest in the USA.

Here a answer from DefectiveByDesign to a probably common question on this topic: _&#8220;Are those two goals (content protection and consumer protection) compatible with one another?&#8221;_

> **DefectiveByDesign:** A better word than consumer, to describe me and you, is citizen. So is content protection and the rights of citizens compatible? When we live in a age where all digital works of art and all human knowledge can be transferred at (next to) zero cost, and where the cost of making one more copy is zero. Is it right to be building digital fences and digital handcuffs around this art and knowledge? If, as citizens of a society, we can see the advantages of allowing art and knowledge to flow without impediment, we as citizens will also have reason to find new ways to recompense the artists and knowledge purveyors. In fact, there are more artists working today than ever before, and more of their art is being enjoyed because of technology free from DRM and free of the Big Media gate keepers. The term &#8220;Content Protection&#8221; is a loaded term, framing the debate with their slant. I would say that this term really describes their attempt to hold back advancing society.

 [1]: http://netzpolitik.org/2006/interview-mit-defectivebydesign-kampagne/
 [2]: http://digitalmusic.weblogsinc.com/2006/08/01/interview-with-defectivebydesign/
 [3]: http://defectivebydesign.org/
 [4]: http://defectivebydesign.org/petition/bonopetition