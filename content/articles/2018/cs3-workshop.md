+++
date = "2018-02-01T19:00:00+02:00"
categories = ["english"]
type = "post"
tags = ["Nextcloud", "cs3", "ocm", "cloud", "federation", "slides"]
image = "articles/slides/cs3-2018/slide1.jpg"
title = "CS3 Workshop 2018 - Global Scale and the future of Federated Cloud Sharing"
summary = "Enable large organisations to scale Nextcloud beyond the typical limitations. As part of Global Scale we will also work on Cloud Federation 2.0, based on the Open Cloud Mesh specification"
location = "Krakow, Poland"
slides = "true"
headerImage = "globalscale.jpg"
+++

At this years [CS3 Workshop in Krakow](http://cs3.cyfronet.pl/) I presented the current state of Nextcloud's Global Scale architecture. Probably the most interesting part of the talk was the current development in the area of Federated Cloud Sharing, a central component of Global Scale. Originally, Federated Cloud Sharing was developed [by Frank Karlitschek and me in 2014](https://www.schiessle.org/articles/2016/07/04/history-and-future-of-cloud-federation/) at ownCloud. These day it enables cloud solutions from ownCloud, Pydio and Nextcloud to exchange files.

As part of Global Scale we will add federated group sharing in the coming months. Further we want to enable apps to provide additional "federated share providers" in order to implement federated calendar sharing, federated contact sharing and more.

The next iteration of Federated Cloud Sharing will be based on the [Open Cloud Mesh (OCM) specification](https://github.com/GEANT/OCM-API). The [Open Cloud Mesh initiative](https://wiki.geant.org/display/OCM/Open+Cloud+Mesh) by GÉANT aims to turn our original idea of Federated Cloud Sharing into a vendor neutral standard. Something I explicitly support. In the process of implementing OCM we will propose some minor changes and additions to the existing specification to meet all our requirements. Directly after my talk I received a lot of positive feedback from different members of the Open Cloud Mesh initiative. I was especially happy to hear that PowerFolder already started to implement OCM as well and that our friends at Seafile also want to join us. I'm looking forward to work together with the OCM-Community in the following weeks and months in order to make our changes part of the official specification.

I will write a more detailed article once we have a first prototype of our implementation. For now I want to share my presentation slides with you:

<div id="cs3-2018"  class="slides">
<ul>
<li><img src="/img/articles/slides/cs3-2018/slide1.jpg" alt="slide0" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide2.jpg" alt="slide1" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide3.jpg" alt="slide2" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide4.jpg" alt="slide3" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide5.jpg" alt="slide4" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide6.jpg" alt="slide5" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide7.jpg" alt="slide6" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide8.jpg" alt="slide7" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide9.jpg" alt="slide8" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide10.jpg" alt="slide9" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide11.jpg" alt="slide10" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide12.jpg" alt="slide11" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide13.jpg" alt="slide12" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide14.jpg" alt="slide13" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide15.jpg" alt="slide14" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide16.jpg" alt="slide15" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide17.jpg" alt="slide16" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide18.jpg" alt="slide17" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide19.jpg" alt="slide18" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide20.jpg" alt="slide19" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide21.jpg" alt="slide20" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide22.jpg" alt="slide21" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide23.jpg" alt="slide22" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide24.jpg" alt="slide23" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide25.jpg" alt="slide24" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide26.jpg" alt="slide25" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide27.jpg" alt="slide26" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide28.jpg" alt="slide27" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide29.jpg" alt="slide28" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide30.jpg" alt="slide29" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide31.jpg" alt="slide30" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide32.jpg" alt="slide31" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide33.jpg" alt="slide32" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide34.jpg" alt="slide33" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide35.jpg" alt="slide34" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide36.jpg" alt="slide35" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide37.jpg" alt="slide36" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide38.jpg" alt="slide37" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide39.jpg" alt="slide38" /></li>
<li><img src="/img/articles/slides/cs3-2018/slide40.jpg" alt="slide39" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

