+++
date = "2020-03-07T08:00:00+02:00"
categories = ["english"]
type = "post"
tags = ["Nextcloud", "fosdem", "cs3", "slides"]
image = "articles/slides/cs3-and-fosdem/slide1.jpg"
title = "Real-time communication and collaboration in a file sync & share environment - A introduction to Nextcloud Talk"
summary = "Nextcloud envolved from a file sync & share solution to a full featured but still modular collaboration platform. Nextcloud Talk is a component which becomes more and more popular and enables Nextcloud users to perform 1:1 or group chats, video and audio calls. This presentation gives a introduction to the latest version of Nextcloud Talk, released together with Nextcloud 18"
location = "Copenhagen, Sweden (CS3) and Brussels, Belgium (FOSDEM)"
slides = "true"
headerImage = "nextcloud.png"
+++

At the beginning of this year I gave two times a presentation about Nextcloud Talk. First at the annual CS3 conference in Copenhagen and just one week later at FOSDEM in Brussels. Nextcloud Talk provides a full featured real-time communication platform. Completely Free Software, self-hosted and nicely integrated with all the other aspects of Nextcloud.

<div id="cs3-and-fosdem"  class="slides">
<ul>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide1.jpg" alt="slide0" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide2.jpg" alt="slide1" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide3.jpg" alt="slide2" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide4.jpg" alt="slide3" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide5.jpg" alt="slide4" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide6.jpg" alt="slide5" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide7.jpg" alt="slide6" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide8.jpg" alt="slide7" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide9.jpg" alt="slide8" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide10.jpg" alt="slide9" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide11.jpg" alt="slide10" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide12.jpg" alt="slide11" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide13.jpg" alt="slide12" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide14.jpg" alt="slide13" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide15.jpg" alt="slide14" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide16.jpg" alt="slide15" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide17.jpg" alt="slide16" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide18.jpg" alt="slide17" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide19.jpg" alt="slide18" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide20.jpg" alt="slide19" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide21.jpg" alt="slide20" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide22.jpg" alt="slide21" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide23.jpg" alt="slide22" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide24.jpg" alt="slide23" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide25.jpg" alt="slide24" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide26.jpg" alt="slide25" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide27.jpg" alt="slide26" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide28.jpg" alt="slide27" /></li>
<li><img src="/img/articles/slides/cs3-and-fosdem/slide29.jpg" alt="slide28" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

