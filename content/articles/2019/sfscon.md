+++
date = "2019-11-17T08:00:00+02:00"
categories = ["english"]
type = "post"
tags = ["fsfe", "sfscon", "freesoftware", "business", "slides"]
image = "articles/slides/sfscon-2019/slide1.jpg"
title = "New challenges for Free Software business models"
summary = "Different Free Software business models evolved over the past and the most successful are threaten these days by the raise of IaaS providers. What does this mean for the future of economical success of Free Software?"
location = "Bolzano, South Tyrol"
slides = "true"
headerImage = "fsfs.jpg"
+++

This year the FSFE community meeting was combined with the "South Tyrol Free Software Conference" (SFScon) in Bolzano. For me this was a special event because the first international FSFE community meeting ever happened as well at the SFScon in 2006. Back then I met many people from FSFE in person for the first time. For me this was the starting point for getting more and more involved in the Free Software Foundation Europe.

At this years conference I gave a talk about the "New challenges for Free Software business models" at the FSFE track. A few weeks ago I published a article about this topic in the [German Linux Magazine](https://www.linux-magazin.de/ausgaben/2019/10/fsfe-standpunkt/). As many of you may know, Free Software as such is not a business model but a license model which can be combined with many different business and development models.

![Distinction between business-, license- and development-model](/img/articles/businessmodels.png "Distinction between business-, license- and development-model")

I'm convinced that working business models around Free Software are a important building block for Free Software to compete successfully with the proprietary software world. The questions how to make money with Free Software and how to build sustainable and strong companies around Free Software are important topics almost right from the beginning of the Free Software movement. Over time we come up with various business models which worked quite well. But the change in technology over the last few years start to put some of the more successful business models at risk. The talk summarized the current challenges and invited the audience to think about possible solutions.

<div id="sfscon-2019"  class="slides">
<ul>
<li><img src="/img/articles/slides/sfscon-2019/slide1.jpg" alt="slide0" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide2.jpg" alt="slide1" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide3.jpg" alt="slide2" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide4.jpg" alt="slide3" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide5.jpg" alt="slide4" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide6.jpg" alt="slide5" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide7.jpg" alt="slide6" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide8.jpg" alt="slide7" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide9.jpg" alt="slide8" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide10.jpg" alt="slide9" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide11.jpg" alt="slide10" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide12.jpg" alt="slide11" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide13.jpg" alt="slide12" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide14.jpg" alt="slide13" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide15.jpg" alt="slide14" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide16.jpg" alt="slide15" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide17.jpg" alt="slide16" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide18.jpg" alt="slide17" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide19.jpg" alt="slide18" /></li>
<li><img src="/img/articles/slides/sfscon-2019/slide20.jpg" alt="slide19" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

The recording can be found [here](https://peertube.social/videos/watch/309583e7-60fc-4aa8-903b-6fdc1cb51f50).

After the talk I had many interesting discussions. Most people agreed that this is a problem. One suggestion was to have a look at Mozilla for a successful business model. Another idea was that the big IaaS providers might buy some of the companies behind the software in the future and continue the development, which wouldn't be a problem as long as they would stick to Free Software. Yet another interesting thought was that if you look at the software market as a whole you will realize that Free Software is still a small piece of the cake. As long as the cake as a whole and the Free Software part in particular grows fast we don't have to worry that much how the Free Software part is split up, there will be enough space for everyone. The e.foundation was also mentioned as a possible example for a successful business model and many more ideas floated around.

I don't want to comment on the individual ideas here but it shows that we had a lively discussion with many interesting ideas and thoughts.

Do you also have some thoughts around this topic? Feel free to share them in the comments!
