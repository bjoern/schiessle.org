---
title: Installing Wallabag 2 on a Shared Web Hosting Service
author: Björn Schießle
type: post
date: 2016-04-16T21:41:06+00:00
slug: installing-wallabag-2-on-a-shared-web-hosting-service
categories:
  - english
tags:
  - wallabag
  - selfhosting
headerImage: articles/wallabag.jpg
headerImageCopyright: Wallabag 2.0.1
---

[Wallabag][1] describes itself as a self hostable application for saving web pages. I&#8217;m using Wallabag already for quite some time and I really enjoy it to store my bookmarks, organize them by tags and access them through many different clients like the web app, the official Android app or the Firefox plug-in. 

Yesterday I updated by Wallabag installation to version 2.0.1. The basic installation was quite easy by following the [documentation][2]. I had only one problem. I run Wallabag on a shared hoster, so I couldn&#8217;t adjust the Apache configuration to redirect the requests to the right sub-directory, as described by the documentation. I solved the problem with a small .htaccess file I added to the root folder: 

<pre lang="bash">&lt;IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{HTTP_HOST} ^links\.schiessle\.org$ [NC]
    RewriteRule !^web/ /web%{REQUEST_URI} [L,NC]
&lt;/IfModule>
</pre>

I also noticed that Wallabag has a &#8220;register&#8221; button which allows people to create a new account. There already exists a [feature request][3] to add a option to disable it. Because I don&#8217;t want to allow random people to register a account on my Wallabag installation I disabled it by adding following additional lines to the .htaccess file:

<pre lang="bash">&lt;FilesMatch ".*register$">
    Order Allow,Deny
    Deny from all
&lt;/FilesMatch>
</pre>

 [1]: https://www.wallabag.org
 [2]: http://doc.wallabag.org/en/v2/user/installation.html
 [3]: https://github.com/wallabag/wallabag/issues/1873
