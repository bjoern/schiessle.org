---
title: Guake Terminal Improvement for Multi-Monitor Setups
author: Björn Schießle
type: post
date: 2016-03-26T19:03:59+00:00
slug: guake-terminal-improvement-for-multi-monitor-setups
categories:
  - english
tags:
  - gtk
  - guake
  - hacking
  - python
headerImage: articles/guake.png
---

[Guake][1] is a top-down &#8220;Quake-style&#8221; terminal. I use it on a daily basis on the Xfce desktop. The only drawback, Guake doesn&#8217;t work the way I want it on a multi-monitor setup. On such a setup the terminal always starts on the main (left) monitor. But for many people, including myself, the left monitor is the small Laptop monitor. Therefor many people prefer to open the terminal on the secondary (right) monitor. If you search for &#8220;Guake multi-monitor&#8221; you can find many patches to achieve this behavior.

For me it is not enough that the terminal always starts on the right monitor. I want the terminal to always start at the currently active monitor, the monitor which contain the mouse pointer. Luckily Guake is written in Python, this makes it quite easy to patch it without the need to re-compile and re-package it. Together with the patches already available on the Internet and a short look at the Gtk documentation I found a solution. To always show the terminal on the currently active monitor you have to edit `/usr/bin/guake` and replace the method `get_final_window_rect(self)` with following code:

<pre lang="python">def get_final_window_rect(self):
        """Gets the final size of the main window of guake. The height
        is the window_height property, width is window_width and the
        horizontal alignment is given by window_alignment.
        """
        screen = self.window.get_screen()
        height = self.client.get_int(KEY('/general/window_height'))
        width = 100
        halignment = self.client.get_int(KEY('/general/window_halignment'))

        # get the rectangle from the currently active monitor
        x, y, mods = screen.get_root_window().get_pointer()
        monitor = screen.get_monitor_at_point(x, y)
        window_rect = screen.get_monitor_geometry(monitor)
        total_width = window_rect.width
        window_rect.height = window_rect.height * height / 100
        window_rect.width = window_rect.width * width / 100

        if width &lt; total_width:
            if halignment == ALIGN_CENTER:
                window_rect.x = (total_width - window_rect.width) / 2
                if monitor == 1:
                    right_window_rect = screen.get_monitor_geometry(0)
                    window_rect.x += right_window_rect.width
            elif halignment == ALIGN_LEFT:
                window_rect.x = 0
            elif halignment == ALIGN_RIGHT:
                window_rect.x = total_width - window_rect.width
        window_rect.y = 0
        return window_rect
</pre>

This patch is based on Guake 0.4.4. The current stable version is already at 0.8.4 and no longer contain the method shown above. Still version 0.4.4 is in use on the current Debian stable version (Jessie), therefore I thought that it might be useful for more people than just for me.

 [1]: http://guake.org/
