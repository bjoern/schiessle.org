---
title: Road Ahead
author: Björn Schießle
type: post
date: 2016-05-26T08:02:32+00:00
slug: road-ahead
categories:
  - english
tags:
  - community
  - future
  - owncloud
  - personal
headerImage: articles/road-ahead.jpg
headerImageCopyright: By Nicholas A. Tonelli (CC BY 2.0)
---

I just realized that at June, 1 it is exactly four years since I joined [ownCloud Inc][1]. That&#8217;s a perfect opportunity to look back and to tell you about some upcoming changes. I will never forget how all this get started. It was FOSDEM 2012 when I met [Frank][2], we already knew each other from various Free Software activities. I told him that I was looking for new job opportunities and he told me about ownCloud Inc. The new company around the [ownCloud][3] initiative which he just started together with the help of others. I was directly sold to the idea of ownCloud and a few months later I was employee number six at ownCloud Inc. 

This was a huge step for me. Before joining ownCloud I worked as a researcher at the University of Stuttgart, so this was the first time I was working as a full-time software engineer on a real-world project. I also didn&#8217;t write any noteworthy PHP code before. But thanks to a awesome community I got really fast into all the new stuff and could speed up my contributions. During the following years I worked on many different aspects of ownCloud, from sharing, over files versions to the deleted files app up to a complete re-design of the server-side encryption. I&#8217;m especially happy that I could contribute substantial parts to a feature called [&#8220;Federated Cloud Sharing&#8221;][4], from my point of view one of the most important feature to move ownCloud to the next level. Today it is not only possible to share files across various ownCloud servers but also between other cloud solutions like Pydio. 

But the technical part is only a small subset of the great experience I had over the last four years. Working with a great community is just amazing. It is important to note that with community I mean everyone, from co-workers and students to people who contributed great stuff to ownCloud in their spare time. We are all ownCloud, there should be no distinction! We not only worked together in a virtual environment but meet regularly in person at Hackathons, various conferences and at the annual ownCloud conference. I met many great people during this time which I can truly call friends today. I think this explains why ownCloud was never just a random job to me and why I spend substantial parts of my spare time going to conferences, giving talks or helping at booths. ownCloud combined all the important parts for me: People, Free Software, Open Standards and Innovation. 

Today I have to announce that I will move on. May, 25 was my last working day at the ownCloud company. This is a goodbye and thank you to ownCloud Inc. for all the opportunities the company provided to me. But it is in no way a goodbye to all the people and to ownCloud as a project. I&#8217;m sure we will stay in contact! That&#8217;s one of many great aspects of Free Software. If it is done right a initiative is much more than any company which might be involved. Leaving a company doesn&#8217;t mean that you have to leave the people and the project behind. 

Of course I will continue to work on Free Software and with great communities, especially I have no plans to leave the ownCloud community. Actually I hope that I can even re-adjust my Free Software and community focus in the future&#8230; Stay tuned.

 [1]: https://owncloud.com/
 [2]: http://karlitschek.de
 [3]: https://owncloud.org/
 [4]: http://blog.schiessle.org/2016/03/14/federated-sharing-whats-new-in-owncloud-9-0/
