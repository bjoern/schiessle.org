---
title: Werkzeugkasten Freie Software
author: Björn Schießle
type: post
date: 2016-12-15T10:44:48+00:00
slug: werkzeugkasten-freie-software
categories:
  - deutsch
tags:
  - education
  - oer
  - school
  - FreeSoftware
headerImage: articles/werkzeugkasten-freie-software.jpg
---

Nach viel Arbeit, die dank einem tollen Autoren-Team und Herausgeber mindestens genauso viel Spaß gemacht hat, freue ich mich das der [Werkzeugkasten Freie Software][2] veröffentlicht wurde. Ganz im Sinn der [Open Education Resources][3] ist der Werkzeugkasten unter der [Creative Commons By-SA Lizenz][4] erhältlich. Damit können alle Inhalte im und außerhalb des Unterrichts frei verwendet werden, solange die Quelle benannt wird. Auch Anpassungen und Erweiterungen sind durch die Lizenz erlaubt. Hierfür steht der vollständige Werkzeugkasten als HTML, PDF, DOCX und ODT zum Download bereit. 

[<img src="/img/articles/werkzeugkasten-freie-software-cover.png" style="float: right" />][5]
  
Das Konzept [Freie Software][6] ist für die Schule wie geschaffen. Freie Software erlaubt es die Software für jeden Zweck zu verwenden, sie zu untersuchen, anzupassen und weiterzugeben. Dadurch können Schüler ohne Hindernisse, wie restriktive Lizenzen, mit der Software ihrer Wahl arbeiten. Durch die Möglichkeit die Software frei zu kopieren ist sicher gestellt, dass alle Schüler die gleichen Chancen haben und der Erfolg im Unterricht nicht vom Geldbeutel der Eltern abhängt. Beim Einsatz Freier Software kann auch der Lehrer bedenkenlos die Software an die Schüler ausgeben, so dass zu Hause und in der Schule mit der selben Software gearbeitet werden kann. Schüler können zusammen lernen und die Software untereinander austauschen, dies ist eine Art der Zusammenarbeit wie sie in vielen anderen Bereichen der Schule selbstverständlich ist. Da Software fachübergreifend in nahezu jedem Bereich eingesetzt wird, kommt ihr eine besondere Bedeutung bei der Frage zu, welche Art des Miteinander und der Zusammenarbeit wir in Schulen vermitteln wollen.   
Sollte die Neugier groß genug sein, ermöglicht Freie Software auch das sich die Schüler das Innenleben der Software anschauen und diese vielleicht sogar selber anpassen. Dadurch können Schüler spielerisch ein besseres Verständnis für die Technologie erwerben die sie im Alltag umgibt. Grundzüge dieser Technologie zu verstehen wird in Zukunft genauso wichtig sein wie das Lesen und Schreiben. Mit Freier Software haben Schulen die einmalige Gelegenheit Schülern nicht nur die Bedienung einer Black-Box beizubringen, sondern zur Erlangung nachhaltigem Wissens beizutragen. 

Proprietäre Software verwendet oft seine ganz eigenen, undokumentierten Dateiformate. Dadurch besteht die Gefahr das man frühzeitig Dokumente in Formaten anhäuft welche man in Zukunft entweder gar nicht mehr lesen kann oder die einen dazu zwingen weiterhin Software des gleiche Herstellers einzusetzen. Dieser Lock-In Effekt ist einer der Gründe warum Hersteller ihre proprietäre Software oft bereitwillig günstig oder vollständig kostenlos an Schulen ausgegeben. Man sollte sich gut überlegen ob man ein solches zweifelhaftes &#8220;Geschenk&#8221; annehmen will. Im Gegensatz dazu basiert Freie Software oft auf standardisierten Dateiformaten. Diese erhöhen die Chancen, dass man seine Dokumente auch noch viele Jahre nach der Erstellung lesen und weiterverarbeiten kann. Auch die Möglichkeiten die Software zu wechseln sind dadurch deutlich besser. 

Ich hoffe, dass es uns mit dem Werkzeugkasten Freie Software gelungen ist sowohl die grundlegenden Prinzipien und Vorteile Freier Software zu vermitteln, also auch ganz konkrete Softwarelösungen und Einsatzmöglichkeiten für den Alltag in der Schule vorzustellen. Ich bin schon sehr gespannt wie das Ergebnis angenommen wird und freue mich über jede Art von Rückmeldung.

 [2]: http://www.medien-in-die-schule.de/werkzeugkaesten/werkzeugkasten-freie-software/
 [3]: https://en.wikipedia.org/wiki/Open_educational_resources
 [4]: https://creativecommons.org/licenses/by-sa/4.0/deed.de
 [5]: http://www.medien-in-die-schule.de/werkzeugkaesten/werkzeugkasten-freie-software/einleitung-werkzeugkasten-freie-software/
 [6]: https://fsfe.org/about/basics/freesoftware.de.html
