---
title: 'Federated Sharing  – What’s new in ownCloud 9.0'
author: Björn Schießle
type: post
date: 2016-03-14T16:03:24+00:00
slug: federated-sharing-whats-new-in-owncloud-9-0
categories:
  - english
tags:
  - federation
  - owncloud

---
Privacy, control and freedom was always one of the main reasons to run your own cloud instead of storing your data on a proprietary and centralized service. Only if you run your own cloud service you know exactly where your data is stored and who can access it. You are in control of your data. But this also introduces a new challenge. If everyone runs his own cloud service it become inevitable harder to share pictures with your friends or to work together on a document. That&#8217;s the reason why we at ownCloud are working at a feature called [Federated Cloud Sharing][1]. The aim of Federated Cloud Sharing is to close this gap by allowing people to connect their clouds and easily share data across different ownCloud installations. For the user it should make no difference whether the recipient is on the same server or not. 

## What we already had

The first implementation of Federated Cloud Sharing was introduced with ownCloud 8.0. Back then it was mainly a extension of the already existing feature to share a file or folder with a public link. People can create a link and share it with their friends or colleagues. Once they open the link in a browser they will see a button called &#8220;Add to your ownCloud&#8221; which enables them to mount the share as a WebDAV resource to their own cloud. 

<img class="article-image center" src="/img/articles/owncloud-mount-public-link-example.png" />

With ownCloud 8.1 we moved on and added the Federated Cloud ID as a additional way to initiate a remote share. The nice thing is that it basically works like a email address. Every ownCloud user automatically gets a ID which looks similiar to `john@myOwnCloud.org`. Since ownCloud 8.2 the users Federated Cloud ID is shown in the personal settings. 

<img src="/img/articles/nextcloud-personal-settings-personal-cloud-id.png" alt="federated-cloud-id" width="713" />

To share a file with a user on a different ownCloud you just need to know his Federated Cloud ID and enter it to the ownCloud share dialog. The next time the recipient log-in to his ownCloud he will get a notification that he received a new share. The user can now decide if he wants to accept or decline the remote share. In order to make it easier to remember the users Federated Cloud ID the Contacts App allows you to add the ID to your contacts. The share dialog will automatically search the address books to auto-complete the Federated Cloud IDs. 

## What&#8217;s new in ownCloud 9.0

With ownCloud 9.0 we made it even easier to exchange the Federated Cloud IDs. Below you can see the administrator setting for the new Federation App, which will be enabled by default. 

<img src="/img/articles/nextcloud-trusted-servers.png" class="article-image center" />

The option &#8220;Add server automatically once a federated share was created successfully&#8221; is enabled by default. This means, that as soon as a user creates a federated share with another ownCloud, either as a recipient or as a sender, ownCloud will add the remote server to the list of trusted ownClouds. Additionally you can predefined a list of trusted ownClouds. While technically it is possible to use plain http I want to point out that I really recommend to use https for all federated share operations to secure your users and their data. 

What does it mean that two ownClouds trust each other? ownCloud 9.0 automatically creates a internal address book which contains all users accounts. If two ownClouds trust each other they will start to synchronize their system address books. In order to synchronize the system address books and to keep them up-to-date we use the well known and widespread CardDAV protocol. After the synchronization was successful ownCloud will know all users from the trusted remote servers, including their Federated Cloud ID and their display name. The share dialog will use this information for auto-completion. This allows you to share files across friendly ownClouds without knowing more than the users name. ownCloud will automatically find the corresponding Federated Cloud ID and will suggest the user as a recipient of your share. 

The screen-shot of the new Federation App shows a status indicator for each server with three different states: green, yellow and red. Green means that both servers are connected and the address book was synced at least once. In this state auto-completion should work. Yellow means that the initial synchronization is still in progress. Creating a secure connection between two ownCloud servers and syncing the users happens in the background. This can take same time, depending on the background job settings of your ownCloud and the settings of the remote server. If the indicator turns red something went wrong in a way that it can&#8217;t be fixed automatically. ownCloud will not try to reestablish a connection to the given server. To reconnect to the remote server you have to remove the server and add it again. 

If the auto-add option is enabled, the network of known and trusted ownClouds will expand every time a user on your server establish a new federated share. The boundaries between local users and remote users will blur. Each user will stay in control of his data, stored on his personal cloud but from a collaborative point of view everything will work as smooth as if all users would be on the same server. 

What will come next? Of course we don&#8217;t want to stop here. We will continue to make it as easy as possible to stay in control of your data and at the same time share your files with all the other users and clouds out there. Therefor we work hard to [document and standardize our protocols][2] and invite other cloud initiatives to join us to create a Federation of Clouds, not only across different ownCloud servers but also across otherwise complete different cloud solutions.

 [1]: https://owncloud.org/federation/
 [2]: http://karlitschek.de/2015/08/announcing-the-draft-federated-cloud-sharing-api/