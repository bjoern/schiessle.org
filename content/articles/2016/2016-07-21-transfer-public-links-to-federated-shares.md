---
title: Transfer Public Links to Federated Shares
author: Björn Schießle
type: post
date: 2016-07-21T09:56:48+00:00
slug: transfer-public-links-to-federated-shares
diaspora_aspect_ids:
  - 'a:1:{i:0;s:11:"all_aspects";}'
categories:
  - english
tags:
  - federation
  - nextcloud
headerImage: articles/nextcloud-transform-public-link.svg
headerImageCopyright: Transform a public link to a federated share
---

Creating public links and sending them to your friends is a widely used feature of [Nextcloud][1]. If the recipient of a public link also has a Nextcloud or ownCloud account he can use the &#8220;Add to your Nextcloud&#8221; button to mount the content over WebDAV to his server. On a technical level all mounted public links use the same token, the one of the public link, to reference the shared file. This means that as soon as the owner removes the public link all mounts will disappear as well. Additionally, the permissions for public links are limited compared to normal shares, public links can only be shared read-only or read-write. This was the first generation of federated sharing which we introduced back in 2014.

A year later we introduced the possibility to create federated shares directly from the share dialog. This way the owner can control all federated shares individually and use the same permission set as for internal shares. Both from a user perspective and from a technical point of view this lead to two different ways to create and to handle federated shares. With Nextcloud 10 we finally bring them together.

## Improvements for the owner

<div class="article-image" style="float: right; width: 200px">
<img src="/img/articles/nextcloud-convert-public-link-to-federated-share.png"
alt="Public Link Converted to a Federated Share" width=200px/>
<p class="image-description">
    Public link converted to a federated share for bjoern@myNextcloud.net
  </p>
</div>

From Nextcloud 10 on every mounted link share will be converted to a federated share, as long as the recipient also runs Nextcloud 10 or newer. This means that the owner of the file will see all the users who mounted his public link. He can remove the share for individual users or adjust the permissions. For each share the whole set of permissions can be used like &#8220;edit&#8221;, &#8220;re-share&#8221; and in case of folder additionally &#8220;create&#8221; and &#8220;delete&#8221;. If the owner removes the original public link or if it expires all federated shares, created by the public link will still continue to work. For older installations of Nextcloud and for all ownCloud versions the server will fall-back to the old behavior.

## Improvements for the user who mounts a public link

<div class="article-image" style="float: left; width: 310px">
<img src="/img/articles/nextcloud-mount-public-link.png"
alt="After opening a public link the user can convert a public link to a federated share by adding his Federated Cloud ID or his Nextcloud URL" width=310px/></a>
<p class="image-description">
   After opening a public link the user can convert it to a federated share by
   adding his Federated Cloud ID or his Nextcloud URL
  </p>
</div>

Users who receive a public link and want to mount it to their own Nextcloud have two options. They can use this feature as before and enter the URL to their Nextcloud to the &#8220;Add to your Nextcloud&#8221; field. In this case they will be re-directed to their Nextcloud, have to login and confirm the mount request. The owners Nextcloud will then send the user a federated share which he has to accept. It can happen that the user needs to refresh his browser window to see the notification.
  
Additionally there is a new and faster way to add a public link to your Nextcloud. Instead of entering the URL to the &#8220;Add to your Nextcloud&#8221; field you can directly enter your federated cloud ID. This way the owners Nextcloud will send the federated share directly to you and redirect you to your server. You will see a notification about the new incoming share and can accept it. Now the user also benefit from the new possibilities of the owner. The owner can give him more fine grained permissions and from the users point of view even more important, he will not lose his mount if the public link gets removed or expires.

Nextcloud 10 introduces another improvement in the federation area: If you re-share a federated share to a third server, a direct connection between the first and the third server will be created now so that the owner of the files can see and control the share. This also improves performance and the potential error rate significantly, avoiding having to go through multiple servers in between.

 [1]: https://nextcloud.com/
