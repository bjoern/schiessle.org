---
title: Freie Software im Koalitionsvertrag Baden-Württemberg
author: Björn Schießle
type: post
date: 2016-05-10T19:30:18+00:00
slug: freie-software-im-koalitionsvertrag-baden-wurttemberg
categories:
  - deutsch
tags:
  - bawü
  - cdu
  - FreeSoftware
  - grüne
  - policy
  - fsfe
  - election
headerImage: articles/baden-wuerttemberg.png
---

Am 13. März wurde in Baden-Württemberg der neue Landtag gewählt. Die nächsten 5 Jahre werden politisch von einer Koalition aus Bündnis 90/Die Grünen und der CDU gestaltet. Am letzten Wochenende wurde hierfür der [Koalitionsvertrag][1] von beiden Parteien bestätigt. Ich nahm diese Gelegenheit zum Anlass, um mir den Koalitionsvertrag genauer anzusehen, insbesondere mit Blick auf [Freie Software][2]. Dabei wurde ich an mehreren Stellen fündig.

So heißt es im Abschnitt &#8220;Chance zur Entbürokratisierung&#8221;:

> Wir werden die E-Government-Richtlinien und das Beschaffungswesen des Landes bei der IT-Beschaffung in Richtung Open Source weiterentwickeln.

Dies ist sehr zu begrüßen. Unter anderem fordern Organisationen wie die [Free Software Foundation Europe (FSFE)][3] oder die [Open Source Business Alliance (OSBA)][4] schon seit längerem, dass durch die Öffentlichkeit finanzierte Software unter einer Freie-Software-Lizenz veröffentlicht werden soll und dass bei Ausschreibungen Freie Software stärker beachtet wird. Gerade in diesem Jahr möchten beide Organisationen hierzu auch verstärkt aktiv werden.

Weiter heißt es im selben Abschnitt:

> Auch die Bereitstellung freier Software und offener Bildungsressourcen (OER) durch das Landesmedienzentrum begrüßen und unterstützen wir.

Gerade Schulen, in denen die nächste Generation mit Software und Bildungsressourcen zum ersten Mal systematisch in Kontakt kommt, ist es von großer Bedeutung, dass von Anfang an ein Verständnis dafür entwickelt wird, wie man im Informationszeitalter nachhaltig Wissen und Information erarbeitet und veröffentlicht. Wie könnte dies besser geschehen als durch den praktischen Einsatz von Freier Software und [freien Lerninhalten][5]?

Im Abschnitt &#8220;Allianz Wirtschaft 4.0 für die Digitalisierung im Mittelstand&#8221; ist sogar ein ganzer Abschnitt Freier Software gewidmet. So heißt es dort:

> Kleine und mittlere IT-Unternehmen im Land sind besonders aktiv in der Entwicklung von freier, quelloffener Software (Open Source) und in den damit verbundenen Dienstleistungen. Open Source bietet ebenso wie freie Standards und offene Formate große Chancen für ein herstellerunabhängiges Software-Ökosystem. Diese Ansätze wollen wir unterstützen.

Hier wird zurecht der Vorteil Freier Software zur Stärkung des Standorts gewürdigt. Freie Software ermöglicht es, lokale Unternehmen zu fördern und sowohl Wissen als auch Wirtschaftsleistung im Land zu halten. Darüber hinaus wird auf die Wichtigkeit eines herstellerunabhängigen Software-Ökosystems hingewiesen. Man darf gespannt sein, wie die konkrete Unterstützung und Förderung in den nächsten Jahren aussehen wird.

Der Abschnitt &#8220;DIGITAL@BW: Schulen mit Digitalisierung und Medienkompetenz&#8221; wird noch einmal ausführlicher auf die Rolle von Freier Software und Open Education Resources (OER) eingegangen:

> Wir werden die pädagogisch begleitete Nutzung von E-Learning-Programmen im Unterricht vorantreiben und ihr Potenzial hin zu einer genau auf den einzelnen Schüler abgestimmten individuellen Förderung erschließen. Digitale Medien sind fächerübergreifend ebenso wie im Fachunterricht hilfreich. Entscheidend ist weniger die Technik als vielmehr das pädagogische Konzept. Wir setzen uns dafür ein, dass an den Schulen verstärkt freie Lern- und Lehrmaterialien (Open Educational Resources und Freie Software) genutzt werden können.

Gerade bei der fortschreitenden Digitalisierung der Schulen besteht die Gefahr, dass mit dem Einsatz von proprietärer Software frühzeitig Produktschulung betrieben wird, anstelle dass Konzepte gelehrt werden. Des Weiteren kann es schnell passieren, dass der Unterricht mehr oder weniger direkt zur Werbung für einzelne Unternehmen und Produkte genutzt wird. Auch darf der Lock-In Effekt nicht unterschätzt werden. Haben Schüler über viele Jahre hinweg gelernt, mit einer bestimmten Software zu arbeiten und viele Dokumente in proprietären Formaten erstellt, wird ein späterer Wechsel viel schwieriger. Dieses Risiko kann gemindert werden, indem die Schulen darauf achten, dass Dokumente in offen standardisierten Formaten erstellt und bereitgestellt werden.

Es ist zu begrüßen, wenn durch den Einsatz von Freier Software und Offenen Standards eine Bindung an einzelne Programme oder Unternehmen verhindert oder zumindest reduziert wird. Dies gelingt natürlich nur, wenn der Unterricht auch entsprechend aufgebaut ist. Die Wahl von freien Werkzeugen und offen Bildungsressourcen sorgen aber schon einmal für gute Grundvoraussetzungen.

Die Bekundungen zu Freier Software, Offenen Standards und offenen Bildungsressourcen hören sich durchweg positiv an. Wie man aus vergangenen Koalitionsverträgen weiß, bedeutet das aber nicht immer, dass auch alles entsprechend umgesetzt wird. Von daher bleibt es spannend zu beobachten was in den nächsten fünf Jahren in Baden-Württemberg im Bezug auf Freie Software passiert. Ich werde es mit großem Interesse verfolgen und freue mich, wenn ich im Laufe dieser Zeit über konkrete Umsetzungen berichten kann.

 [1]: https://www.baden-wuerttemberg.de/de/regierung/landesregierung/koalitionsvertrag/
 [2]: https://fsfe.org/about/basics/freesoftware.de.html
 [3]: https://fsfe.org/activities/procurement/
 [4]: http://rdir.de/form.do?agnCI=856&agnFN=fullview&agnUID=nc.A.B.BEmP.Cfw.BPMwR.t2Q_9BaKGebAGx4_ENwQhw
 [5]: https://de.wikipedia.org/wiki/Open_Educational_Resources
