---
title: "Send me attachments I can read, use Open Standards!"
layout: "static"
---

# Send me attachments I can read, *use Open Standards!*

There are many ways to share documents, files and data over the Internet. Among them, emails are often used because people can communicate from one mail server to another without any difficulty. Why does it work so simply? Because **emails are designed to use a set of Open Standards**, based on the Internet protocols.

However, sometimes people send attachments along with their emails, and it happens frequently that the attachments cannot be read by the recipients. For example, many attached files are documents produced by word processors and it can be impossible to read them correctly if you do not have the same word processor. Many proprietary word processors use proprietary file formats.

When you attach a file to an email, please make sure that your correspondent will be able to read your files correctly. It is a basic principle of courtesy. And there is an easy way to make this possible: use Open Standards. If you do so, your correspondent will have the possibility to choose which program he or she wants. <strong>Open Standards guarantee sustainability and interoperability</strong> for your data, making sure you will be able to access them in the future, even with another software, on another platform or operating system.

There are two easy rules to make sure that every recipient can read the document you send them:

1. If the recipient should **only read** the content, which is the case most of the time, put it directly as text into your email or convert the document to a **PDF** and send the PDF. This also has the nice side effect, that you can be sure that the document looks the same for everyone.
2. If you want the recipient to **edit the document**, store the document in the **Open Document Format (odt)**, even Mircosoft Office, offers these option today.

By promoting Open Standards you will help everyone: <strong>sharing documents can be as easy as sending and receiving emails!</strong> If you receive an email using proprietary file formats, don't hesitate to share this page and explain why it is important to use [Open Standards](https://fsfe.org/freesoftware/standards/).

## Campaigns for Open Standards

- [Document Freedom Day](http://www.documentfreedom.org/)
- [PDFreaders.org](http://www.pdfreaders.org">PDFreaders.org)

On <a href="http://documentfreedom.org">Document Freedom Day</a>, the FSF started a campaign to call on computer users to start politely <a href="http://www.fsf.org/news/why-im-rejecting-your-email-attachment">rejecting proprietary attachments</a>.


## Organisations and Software supporting Open Standards</h2>

- <a href="https://www.documentfoundation.org/">The Document Foundation</a>
- <a href="http://www.digitalfreedomfoundation.org/">Digital Freedom Foundation</a>
- <a href="https://fsfe.org/projects/os/">Free Software Foundation Europe</a>

