---
headerimage: "/img/present.jpg"
quote: "if I have seen further, it is by standing on the shoulders of giants."
quoteauthor: "Isaac Newton"
---

Björn gives regular talks about various IT topics. Most of the time it is connected to his day-to-day job at Nextcloud or his volunteery work at FSFE. If you are interested in topics around Free Software, licensing, cloud computing and the technical, social, political and technical implications of modern IT, feel free to [reach out to me](/contact).
