---
title: "Publications"
layout: "static"
headerimage: "/img/bib.jpg"
quote: "What you do makes a difference, and you have to decide what kind of difference you want to make."
quoteauthor: "Jane Goodall"
---

Most of my publications are in German, but some are also in English. Some of the articles where written just by myself, others together with co-authors. Well know experts in their field of expertise. The articles have been published in online magazines, print magazines, books and as teaching reasourses. If you are looking for an author for your next project, don't hesitate to [contact me](/contact).

# Publications

- [Herausforderungen für Open-Source-Geschäftsmodelle]("https://www.linux-magazin.de/ausgaben/2019/10/fsfe-standpunkt/) - Erschienen im Linux-Magazin 10/2019 - Freie Software gibt es seit über 30 Jahren. In dieser Zeit haben sich viele Geschäftsmodelle rund um freie Software herausgebildet und immer wieder verändert. Doch die vielleicht größte Herausforderung steht gerade jetzt an. Neben der Wissenschaft bezieht auch die FSFE dazu Stellung.
- [Die Abkehr von freier und unabhängiger Software in München wäre falsch](https://netzpolitik.org/2017/kommentar-die-abkehr-von-freier-und-unabhaengiger-software-in-muenchen-waere-falsch/) - Kommentar auf Netzpolitik.org (2017)
- [Werkzeugkasten Freie Software](http://www.medien-in-die-schule.de/werkzeugkaesten/werkzeugkasten-freie-software/) - Einführung in die Grundlagen Freier Software zusammen mit einer Sammlung von sinnvoller Software, mit der Unterricht in einem digitalen Zeitalter gestaltet werden kann. Autoren: Volker Grassmuck, Daniel Rohde-Kage, Björn Schießle, Stefan Schober, Sebastian Seitz, Wolf-Dieter Zimmermann (2016)
- [Software Heritage – Erhalt eines Kulturerbes](https://netzpolitik.org/2016/software-heritage-erhalt-eines-kulturerbes/) - Gastbeitrag auf Netzpolitik.org (2016)
- [10 Jahre Sony Rootkit – ein fragwürdiges Jubiläum](https://netzpolitik.org/2015/10-jahre-sony-rootkit-ein-fragwuerdiges-jubilaeum/) - Gastbeitrag auf Netzpolitik.org (2015)
- [User Data Manifesto 2.0](https://netzpolitik.org/2015/user-data-manifesto-2-0/) - Gastbeitrag auf Netzpolitik.org (2015)
- Defensive Publication: Cloud file syncing encryption system. Hugo Roy and Björn Schießle (2014)
- Freie Software – Ein wachsender Faktor in Wirtschaft und Gesellschaft. Erschienen im Handbuch der Unternehmensberatung, 2013, Band 2, Kennzahl 3410, Erich Schmidt Verlag
- [Free Software, Open Source, FOSS, FLOSS – Same same but different](https://fsfe.org/freesoftware/basics/comparison) - Article about the differences and history of the various terms used in the Free Software movement.
- Creating and using roboearth object models. CRA, pages 3549–3550, 2012. Daniel Di Marco, Andreas Koch, Oliver Zweigle, Kai Häussermann, Björn Schiessle, Paul Levi, Dorian Gálvez-López, Luis Riazuelo, Javier Civera, J. M. M. Montiel, Moritz Tenorth, Alexander Clifford Perzylo, Markus Waibel and René van de Molengraft
- Server-sided automatic map transformation in roboearth. AMS, pages 203–216, 2012. Alexander Clifford Perzylo, Björn Schießle, Kai Häussermann, Oliver Zweigle, Paul Levi, and Alois Knoll.
- RoboEarth - A World Wide Web for Robots. IEEE Robotics and Automation Magazine (Special Issue Towards a WWW for Robots), 2011. Accepted - Waibel, M., Beetz, M., D’Andrea, R., Janssen, R., Tenorth, M., Civera, J., Elfring, J., Galvez-Lopez, D., Häussermann, K., Montiel, J.M.M., Perzylo, A., Schießle, B., Zweigle, O., and van de Molengraft, R. (2011)
- Using Situation Analysis Techniques with Bayesian Networks for Recognizing Hardware Errors on Mobile Robots - Oliver Zweigle, Kai Häussermann, Björn Schiessle and Paul Levi (ICRA 2011)


# Quotes and Mentions

- [40 Jahre GNU: Grundsatzfragen zu KI, Red Hat und Co.](https://www.heise.de/news/40-Jahre-GNU-Grundsatzfragen-zu-KI-Red-Hat-und-Co-9321783.html) - "Die Skepsis gegenüber Large-Language-Modellen (LLMs) teilt auch Björn Schießle, der erste externe Gratulant beim GNU Geburtstagstreffen. Der Mitbegründer von Next Cloud sieht aber zugleich eine Chance in der Entwicklung. Die Sorge vor dem Abfluss von Firmendaten durch Cloud-LLMs mache das Thema Souveränität auch für solche Unternehmen attraktiv, die bislang kein Problem mit dem Einsatz proprietärer Software gehabt hätten. "Wir können mit einer guten Antwort auf der Basis von freier Software aufwarten und können eine ganz neue Nutzergruppe gewinnen" erklärte Schießle."
- [Richard Stallman Talks Red Hat, AI, and Ethical Software Licenses at GNU Birthday Event](https://fossforce.com/2023/10/richard-stallman-talks-red-hat-ai-and-ethical-software-licenses-at-gnu-birthday-event/) - "The speakers’ roster included the likes of Nextcloud co-founder Björn Schießle who talked about 'The Next 40 years of Free Software'"

