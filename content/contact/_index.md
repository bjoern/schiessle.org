---
title: "Contact"
layout: "static"
---

# Contact

Below you find my Email addresses and other options to get in contact with me. Most of the time I prefer asynchronous communication. Therefore I recommend to send an Email unless it is really time critical.

## Email addresses

<i class="fa fa-envelope" aria-hidden="true"></i> <a href="&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;&#x3A;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x40;&#x66;&#x73;&#x66;&#x65;&#x2E;&#x6F;&#x72;&#x67;">&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x40;&#x66;&#x73;&#x66;&#x65;&#x2E;&#x6F;&#x72;&#x67;</a>, if you want to talk about Free Software and my work at FSFE.

<i class="fa fa-envelope" aria-hidden="true"></i> <a href="&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;&#x3A;&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#xA;&#x40;&#x6E;&#x65;&#x78;&#x74;&#x63;&#x6C;&#x6F;&#x75;&#x64;&#x2E;&#x63;&#x6F;&#x6D;">&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x6E;&#x65;&#x78;&#x74;&#x63;&#x6C;&#x6F;&#x75;&#x64;&#x2E;&#x63;&#x6F;&#x6D;</a>, if you want to talk about Nextcloud.

<i class="fa fa-envelope" aria-hidden="true"></i> <a href="&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;&#x3A;&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x2E;&#x6F;&#x72;&#x67;">&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x2E;&#x6F;&#x72;&#x67;</a>, for everything else.

## Real-Time Chat

<i class="fa fa-xmpp" aria-hidden="true"></i> <a href="&#x78;&#x6D;&#x70;&#x70;&#x3A;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x40;&#x66;&#x73;&#x66;&#x65;&#x2E;&#x6F;&#x72;&#x67;">&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x40;&#x66;&#x73;&#x66;&#x65;&#x2E;&#x6F;&#x72;&#x67;</a>, if it is not time critical please consider writing a Email instead.

<i class="fa fa-matrix-org" aria-hidden="true"></i> <a href="&#x68;&#x74;&#x74;&#x70;&#x73;&#x3A;&#x2F;&#x2F;&#x6D;&#x61;&#x74;&#x72;&#x69;&#x78;&#x2E;&#x74;&#x6F;&#x2F;&#x23;&#x2F;&#x40;&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x3A;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x2E;&#x6F;&#x72;&#x67;">&#x40;&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x3A;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x2E;&#x6F;&#x72;&#x67;</a>, if it is not time critical please consider writing a Email instead.

<i class="fa signal-messanger" aria-hidden="true"></i> <a href="https://signal.me/#eu/9NlGlFCDV6jizDpcLiOn0-G3NdnCv7GtbNQ4VHhsJ6Tm2vygkbVH9onlhkyjfODY">Signal</a>, if it is not time critical please consider writing a Email instead.

### Related information

- [Please read this for private and secure communication](/privacy/)
- [Please read this if you want to send me an attachment](/attachments/)
