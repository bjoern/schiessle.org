---
headerimage: "/img/urbach.jpg"
title: "Politik. Grün."
layout: "static"
draft: true
---

# Politik. Grün.


## Ein paar Stationen meiner parteipolitischen Aktivitäten

- Mitglied seit 2018 im [Ortsverband Schorndorf](https://gruene-schorndorf.de/), meiner Heimatsstadt.
- Betreiber der Seite unserer [Ortsgruppe in Urbach](https://gruene-urbach.de)
- Mitglied bei der [Netzbegrünung](https://netzbegruening.de)
- [Kreistagskandidat 2019](https://gruene-urbach.de/2019/03/kreistagswahl/) für den Rems-Murr Kreis
- Mitglied des Wahlkampfteams für die Komunalwahlen 2024
- [Kandidat](https://gruene-urbach.de/kandidatinnen) für den Gemeinderat in Urbach 2024 (Listenplatz 12)

## Gemeinderatswahlen 2024

![](/img/komunalwahlen-2024.png)

## Kontakt

Gute Politik kann nur im Dialog mit den Bürger*innen entstehen!

Schreibt mir eine  <a href="&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;&#x3A;&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x2E;&#x6F;&#x72;&#x67;?subject=Gemeinderatswahl 2024">E-Mail</a> oder auf <a href="https://signal.me/#eu/9NlGlFCDV6jizDpcLiOn0-G3NdnCv7GtbNQ4VHhsJ6Tm2vygkbVH9onlhkyjfODY">Signal</a> mit den Themen die euch bewegen!

## Aktuelles

Ihr könnt mir auf [Mastodon](https://gruene.social/@bjoern) folgen, hier ein kleiner Auszug meiner letzten Beiträge:

<div id="mt-container" class="mt-container">
  <div class="mt-body" role="feed">
    <div class="mt-loading-spinner"></div>
  </div>
</div>

