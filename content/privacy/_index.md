---
title: "Secure and private communication, use encryption!"
layout: "static"
quote: "Privacy is a right like any other. You have to exercise it or risk losing it."
quoteauthor: "Phil Zimmermann"
---

# Secure and private communication, *use encryption!*

The real world equivalent of an e-mail is a postcard. If we send a postcard we know that everyone who get in touch with it can read it, therefore often we chose to write a letter instead. By default, the same is true for e-mails and instance messages. It is important to keep this in mind. Especially because this messages pass through many servers before they arrive at the inbox of the recipient. This means that many people are able to copy and read our messages. Since Snowden it is no longer a rumor but a fact that many providers and public authorities store and analyse this messages.

There are tools which allows us to keep our communication private. I highly recommend to look into this possibilities and use encryption whenever possible. On this page you find everything you need to send me secure and private messages.

## Send me a private e-mail

If you want to send me a e-mail in a secure and private way you can use the GNU Privacy Guard (GnuPG). This is a tool available for almost all e-mail clients and even for some web mailer. Read this [tutorial](https://emailselfdefense.fsf.org/en/) if you want to learn how to use it.

My GnuPG Key: *[0x2378A753E2BF04F6](/data/gpg-key.txt)*

My GnuPG Fingerprint: *244F CEB0 CB09 9524 B21F B896 2378 A753 E2BF 04F6*

## Let's chat in a secure and private way

Consider enabling end-to-end encryption if you reach out to me over for Matrix. If you have my mobile phone number you can also use [Signal](https://signal.org).
