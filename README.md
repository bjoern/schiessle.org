This repository contain my homepage schiessle.org.

## On first checkout

After first checkout you have to fetch the submodule with the theme:

````
git submodule update --init
````

## How to build it

1. cd ~/Repos/schiessle.org
2. rm -rf public
3. hugo 
4. rsync -hrvz --delete --exclude=_ public/ schiesbn@schiessle.org:/home/schiesbn/html/

Usually I put this in a bash alias:

````
alias deploy-homepage="cd ~/Repos/build.schiessle.org; git pull; git submodule update --remote; rm -rf public; hugo; rsync -hrvz --delete --exclude=_ public/ schiesbn@schiessle.org:/home/schiesbn/html/; cd -"
````

**Note:** I always build the web page in a separate checkout (in this example `~/Repos/build.schiessle.org`) to avoid to put stuff online which was not yet committed.

## Integration my latest Mastodon posts to the index.html

This is done by a PHP script in the back-end. In order to make it work you have to perform following steps:

1. copy `static/mastodon.feed/config.feed.php` to `/home/schiesbn/mastodon.feed/config.feed.php`. (This path is hard coded at the moment, if you want to use a different path you have to change the `require_once '/home/schiesbn/mastodon.feed/config.feed.php';` statement at the top of `static/mastodon.feed/getmastodonfeed.php`)
2. update the config file with the data and your Mastodon account(s)

The token can be generated in the developer section of your mastodon account settings. It is a bit harder to find your account ID. One way to find it is with following command:

`curl 'https://mastodon.schiessle.org/api/v2/search?q=bjoern&resolve=true&limit=5'  -H 'Authorization: Bearer <your-access-token>`
